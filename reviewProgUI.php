<!DOCTYPE html>
<?php
    session_start();
    ob_start();
    
    $_SESSION['displayMsg'] = "";
    
    include("navbarUI.php");
    include("progController.php");
    
    $rProg = new Programme();
    
    if(isset($_POST['approveBut'])){
          $progID = key($_POST['approveBut']);
          $_POST['progID'] = $progID;

     }
     elseif(isset($_POST['rejectBut'])){
         $progID = key($_POST['rejectBut']);
         $_POST['progID'] = $progID;
     }
     else{
         $progID = 0;
     }
?>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/main.css">
        <title>Review Programmes</title>
    </head>
    <body>
        <!--<div class="text-center">
            <h3>Review Events</h3>
            <p>Listing all newly added events for your approval.</p>
        </div>-->
       
        <section class='reviewProgs'>
            <?php $rProg->displayReviewProg(); ?>
            
            <?php 
                if(isset($_POST['approveBut'])){
                    $userID = $_SESSION['userID'];
                    $progID = $_POST['progID'];
                    $status = 'Approved';
                    $rProg->updateProgReviewStatus($progID, $userID, $status);
                    header("Refresh:0");
                }
                if(isset($_POST['rejectBut'])){
                    $userID = $_SESSION['userID'];
                    $progID = $_POST['progID'];
                    $status = 'Rejected';
                    $rProg->updateProgReviewStatus($progID, $userID, $status);
                    header("Refresh:0");
                }
            ?>
        </section>
           
    </body>
</html>
