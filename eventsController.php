<?php

class Event {

    public $conn;

    function __construct() {
        include ("SIMOpenHouseDB.php");
        $this->conn = $conn;
    }

    function displayAllEvents() {
        $this->checkEventOver();

        $sql = "SELECT * FROM events WHERE eventStatus = 'Upcoming' AND reviewStatus = 'Approved'";
        $qRes = sqlsrv_query($this->conn, $sql);

        date_default_timezone_set('Asia/Singapore');

        echo "<link rel='stylesheet' href='css/eventsCSS.css'>";
        echo "<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>";


        if (isset($_SESSION['userType'])) {
            if ($_SESSION['userType'] == 'SIMStaff' || $_SESSION['userType'] == 'SIMPartner') {

                // ADD EVENT FORM
                echo "<div class='col-sm-2 pull-right'>";
                echo "<form action='manageEventUI.php' method='POST'>";
                echo "<button type='submit' name='addBut' class='btn btn-success'>Add Event </button>";
                echo "</form>";
                echo "</div>";
                // END OF ADD EVENT FORM
            }
        }



        echo "<div class='container-fluid d-flex h-100'>";

        while ($row = sqlsrv_fetch_array($qRes)) {

            //echo "<div class='space'>";
            echo "</div>";
            echo "<div class='col-sm-12'>";
            echo "<div class='well'>";
            echo "<h4>" . $row['eventName'] . "</h4>";
            echo "<div class='eventContainer'>";


            // START OF CALENDAR WIDGET
            echo "<div class='pull-left' style='padding-right: 20px; padding-left:20px; padding-bottom:10px'>";
            //echo "<time datetime='" . date('d F Y', strtotime($row['eventDate'])) . "' class='icon'>";
            echo "<time datetime='" . date_format($row['eventDate'],'d F Y') . "' class='icon'>";
            //echo "<strong>" . date('F Y', strtotime($row['eventDate'])) . "</strong>";
            echo "<strong>" . date_format($row['eventDate'],'F Y') . "</strong>";
            //echo "<span>" . date('d', strtotime($row['eventDate'])) . "</span>";
            echo "<span>" . date_format($row['eventDate'],'d') . "</span>";
            echo "</time>";
            echo "</div>"; 

            // END OF CALENDAR WIDGET
            //echo "<p class='labels'>" . date('d F Y', strtotime($row['eventDate'])). "</p>";
            echo "<p class='labels'><span><i class='fa fa-clock-o' aria-hidden='true'></i></span> " . date_format($row['eventDate'], 'h:ia') . "</p>";
            //echo "<p class='labels'><span><i class='fa fa-clock-o' aria-hidden='true'></i></span> " . date_format($row['eventDate'], 'Y-m-d H:i:s') . "</p>"; 

            echo " | <strong>" . $row['eventAcadInst'] . "</strong> ";

            if ($row['eventStatus'] == "Upcoming") {
                echo "|<strong> " . $row['eventStatus'] . "</strong></p>";
            }
            else {
                echo "<p class='otherStatus'> | " . $row['eventStatus'] . "</p>";
            }


            echo "<p>" . $row['eventDesc'] . "</p>";




            if (isset($_SESSION['userType'])) {
                if ($_SESSION['userType'] == 'Student' xor $_SESSION['userType'] == 'Parent' xor $_SESSION['userType'] == 'Others') {

                    //CHECK IF USER HAS ALREADY SIGNED UP, CHANGE BUTTON ACCORDINGLY
                    echo "<form action='viewAllEventsUI.php' method='POST'>";
                    $this->checkSignedUp($row['eventID'], $_SESSION['userID']);
                    echo "</form>";
                }
                elseif ($_SESSION['userType'] == 'SIMStaff' || $_SESSION['userType'] == 'SIMPartner') {

                    echo "<div class='btn-toolbar'>";
                    // VIEW ATTENDEE LIST FORM
                    echo "<form action='viewAttendeesUI.php' method='POST'>";
                    echo "<input name='eventName' type='text' value='" . $row['eventName'] . "' hidden >";
                    echo "<button name='viewAttendBut[" . $row['eventID'] . "]' type='submit' class='btn btn-info'>View Attendees </button>";
                    echo "</form>";
                    // END OF ATTENDEE LIST FORM
                    // IF USER IS SIM PARTNER -> ONLY ALLOWED TO MANAGE THEIR OWN EVENTS
                    if ($_SESSION['userType'] == 'SIMPartner') {
                        //WILL MODIFY AFTER REGISTRATION PAGE FOR SIM PARTNER IS DONE
                        if ($_SESSION['AwardingInstitute'] == $row['eventAcadInst']) {
                            // MANAGE EVENT FORM
                            echo "<form action='manageEventUI.php' method='POST'>";
                            echo "<button name='mgBut[" . $row['eventID'] . "]' type='submit' class='btn btn-info'>Manage Event </button>";
                            echo "</form>";
                            // END OF MANAGE EVENT FORM
                            echo "</div>";
                        }
                        else {
                            echo "</div>";
                        }
                    }
                    elseif ($_SESSION['userType'] == 'SIMStaff') {
                        // MANAGE EVENT FORM
                        echo "<form action='manageEventUI.php' method='POST'>";
                        echo "<button name='mgBut[" . $row['eventID'] . "]' type='submit' class='btn btn-info'>Manage Event </button>";
                        echo "</form>";
                        // END OF MANAGE EVENT FORM
                        echo "</div>";
                    }
                }
            }
            else {
                // IF USER NOT LOGGED IN, DISPLAY THE FOLLOWING
                echo "<form action='loginUI.php' method='POST'>";
                echo "<button class='btn btn-info' name='login' type='submit'> Login to Sign Up </button>";
                echo "</form>";
            }

            echo "</div>";
            echo "</div>";
            echo "</div>";
        }
    }

    function displayPastEvents() {

        $sql = "SELECT * FROM events WHERE eventStatus = 'Over' OR eventStatus='Cancelled'";
        $qRes = sqlsrv_query($this->conn, $sql);

        date_default_timezone_set('Asia/Singapore');


        echo "<link rel='stylesheet' href='css/eventsCSS.css'>";
        echo "<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>";

        echo "<div class='container-fluid d-flex h-100'>";

        while ($row = sqlsrv_fetch_array($qRes)) {

            echo "<div class='col-sm-12'>";
            echo "<div class='well'>";
            echo "<h4>" . $row['eventName'] . "</h4>";
            echo "<div class='eventContainer'>";

            // START OF CALENDAR WIDGET
            echo "<div class='pull-left' style='padding-right: 20px; padding-left:20px; padding-bottom:10px'>";
            //echo "<time datetime='" . date('d F Y', strtotime($row['eventDate'])) . "' class='icon'>";
            echo "<time datetime='" . date_format($row['eventDate'],'d F Y') . "' class='icon'>";
            //echo "<strong>" . date('F Y', strtotime($row['eventDate'])) . "</strong>";
            echo "<strong>" . date_format($row['eventDate'],'F Y') . "</strong>";
            //echo "<span>" . date('d', strtotime($row['eventDate'])) . "</span>";
            echo "<span>" . date_format($row['eventDate'],'d') . "</span>";
            echo "</time>";
            echo "</div>"; 

            // END OF CALENDAR WIDGET
            //echo "<p class='labels'>" . date('d F Y', strtotime($row['eventDate'])). "</p>";
            echo "<p class='labels'><span><i class='fa fa-clock-o' aria-hidden='true'></i></span> " . date_format($row['eventDate'], 'h:ia') . "</p>";
            //echo "<p class='labels'><span><i class='fa fa-clock-o' aria-hidden='true'></i></span> " . date_format($row['eventDate'], 'Y-m-d H:i:s') . "</p>"; 

            echo "| <strong>" . $row['eventAcadInst'] . "</strong> ";
            echo "|<strong> " . $row['eventStatus'] . "</strong></p>";
            echo "<p>" . $row['eventDesc'] . "</p>";


            if ($_SESSION['userType'] == 'SIMStaff' || $_SESSION['userType'] == 'SIMPartner') {

                echo "<div class='btn-toolbar'>";
                // VIEW ATTENDEE LIST FORM
                echo "<form action='viewAttendeesUI.php' method='POST'>";
                echo "<input name='eventName' type='text' value='" . $row['eventName'] . "' hidden >";
                echo "<button name='viewAttendBut[" . $row['eventID'] . "]' type='submit' class='btn btn-info'>View Attendees </button>";
                echo "</form>";
                // END OF ATTENDEE LIST FORM
                // IF USER IS SIM PARTNER -> ONLY ALLOWED TO MANAGE THEIR OWN EVENTS
                if ($_SESSION['userType'] == 'SIMPartner') {
                    // WILL MODIFY AFTER REGISTRATION PAGE FOR SIM PARTNER IS DONE
                    if ($_SESSION['AwardingInstitute'] == $row['eventAcadInst']) {

                        // MANAGE EVENT FORM
                        echo "<form action='manageEventUI.php' method='POST'>";
                        echo "<button name='mgBut[" . $row['eventID'] . "]' type='submit' class='btn btn-info'>Manage Event </button>";
                        echo "</form>";
                        // END OF MANAGE EVENT FORM
                        echo "</div>";
                    }
                    else {
                        echo "</div>";
                    }
                }
                elseif ($_SESSION['userType'] == 'SIMStaff') {
                    // MANAGE EVENT FORM
                    echo "<form action='manageEventUI.php' method='POST'>";
                    echo "<button name='mgBut[" . $row['eventID'] . "]' type='submit' class='btn btn-info'>Manage Event </button>";
                    echo "</form>";
                    // END OF MANAGE EVENT FORM
                    echo "</div>";
                }
            }
            echo "</div>";
            echo "</div>";
            echo "</div>";
        }
    }

    function checkSignedUp($eventID, $userID) {
        $userID = (int) $userID;
        $eventID = (int) $eventID;

        date_default_timezone_set('Asia/Singapore');
        $now = new DateTime();

        $sql = "SELECT COUNT(*) as num FROM eventrecords WHERE userID = " . $userID . " AND eventID = " . $eventID;
        $qRes = sqlsrv_query($this->conn, $sql);

        while ($row1 = sqlsrv_fetch_array($qRes)) {
            if ($row1['num'] == 0) {
                echo "<button class='btn btn-info' name='signedUp[" . $eventID . "]' type='submit'> Sign Up </button>";
            }
            elseif ($row1['num'] > 0) {

                $sql3 = "SELECT * FROM events WHERE eventID = $eventID";

                $qRes3 = sqlsrv_query($this->conn, $sql3);

                while ($row3 = sqlsrv_fetch_array($qRes3)) {
                    if ($row3['eventDate'] > $now) {
                        echo "<button class='btn btn-info' name='signedUp[" . $eventID . "]' type='submit' disabled> Already Signed Up </button>";
                    }
                    else {
                        echo "<button class='btn btn-danger' name='cancelSignUp[" . $eventID . "]' type='submit'> Cancel Sign Up </button>";
                    }
                }
            }
        }
    }

    function signUpEvent($eventID, $userID) {
        $sql = "INSERT INTO eventrecords (eventID, userID) VALUES ($eventID, $userID)";

        $qRes = sqlsrv_query($this->conn, $sql);
        if ($qRes === FALSE) {
            $_SESSION['displayMsg'] = "There has been an error signing up, please try again." . "Error message" . $this->conn->connect_errno . ": " . $this->conn->connect_error . "\n";
            return false;
        }
        else {
            $_SESSION['displayMsg'] = "You have successfully signed up!";
            return true;
        }
    }

    function cancelSignUp($eventID, $userID) {
        $sql2 = "DELETE FROM eventrecords WHERE eventID = $eventID AND userID = $userID";
        $qRes2 = sqlsrv_query($this->conn, $sql2);

        if ($qRes2 == FALSE) {
            $_SESSION['displayMsg'] = "There has been an error deleting removing your sign up record, please try again." . "Error message" . $this->conn->connect_errno . ": " . $this->conn->connect_error . "\n";
            return false;
        }
        else {
            $_SESSION['displayMsg'] = "<p>You are no longer signed up!</p>";
            return true;
        }
    }

    function addEventDetails() {
        date_default_timezone_set('Asia/Singapore');
        //echo date('Y-m-d\TH:i:s');
        $now = new DateTime();


        echo "<h3 style='text-align:center;'>Add Event</h3>";
        echo "<div class='col pull-right' style='padding-right:20px;'>";
        echo "<a href='viewAllEventsUI.php' class='previous'>&laquo; Return to Events</a>";
        echo "</div>";
        echo "<br/><hr/>";
        echo "<form action='manageEventUI.php' method='POST' style='padding-left: 30px; padding-right:30px;'>";

        echo "<div class='form-group'>";
        echo "<label for='eventName'>Event Name:</label>";
        echo "<input name='eventName' type='text' class='form-control' value='' required>";
        echo "</div>";


        echo "<div class='form-group'>";
        echo "<label for='eventDate'>Event Date:</label>";
        echo "<input name='eventDate' type='datetime-local' class='form-control' value='' min=" . date_format($now,'Y-m-d') . "T00:00 required>";
        echo "</div>";

        if ($_SESSION['userType'] == 'SIMPartner') {
            echo "<div class='form-group'>";
            echo "<label for='eventAcadInst'>Event Academic Institution:</label>";
            echo "<select class='form-control' name='eventAcadInst'>";
            //WILL MODIFY AFTER SIM PARTNER REGISTRATION PAGE IS DONE
            echo "<option value='" . $_SESSION['AwardingInstitute'] . "' selected>" . $_SESSION['AwardingInstitute'] . "</option>";
            echo "</select>";
            echo "</div>";
        }
        elseif ($_SESSION['userType'] == 'SIMStaff') {
            echo "<div class='form-group'>";
            echo "<label for='eventAcadInst'>Event Academic Institution:</label>";
            echo "<select class='form-control' name='eventAcadInst'>";

            $listUnis = array("RMIT University", "University at Buffalo", "University of Stirling", "La Trobe University", "University of Wollongong", "The University of Sydney", "University of Birmingham", "University of London", "SIM Global Education", "The University of Warwick", "Monash College", "Grenoble Ecole de Management");

            foreach ($listUnis as $uni) {
                echo "<option value='" . $uni . "'>" . $uni . "</option>";
            }

            echo "</select>";
            echo "</div>";
        }

        echo "<div class='form-group'>";
        echo "<label for='eventStatus'>Event Status:</label>";
        echo "<select class='form-control' name='eventStatus'>";
        echo "<option value='Upcoming' selected>Upcoming</option>";
        echo "<option value='Over'>Over</option>";
        echo "<option value='Cancelled'>Cancelled</option>";
        echo "</select>";
        echo "</div>";


        echo "<div class='form-group'>";
        echo "<label for='eventDesc'>Event Desc:</label>";
        echo "<textarea name='eventDesc' name='paragraph_text' cols='50' rows='10' class='form-control' required></textarea>";
        echo "</div>";
        
        echo "<div class='form-group'>";
        echo "<input name='postedBy' type='text' value='".$_SESSION['userID']."' hidden>";
        
        if($_SESSION['userType'] == 'SIMStaff'){
            echo "<input name='reviewStatus' type='text' value='Approved' hidden>";
            echo "<input name='moderatedBy' type='text' value='".$_SESSION['userID']."' hidden>";
        }
        elseif($_SESSION['userType'] == 'SIMPartner'){
            echo "<input name='reviewStatus' type='text' value='Pending' hidden>";
        }

        echo "<div class='text-center'>";
        echo "<button name='addEventBut' type='submit' class='btn btn-success'>Add Event</button>";
        echo "</div>";

        echo '</form>';
    }

    function addEvent() {

        $newEName = $_POST['eventName'];
        //$newEDate = date("Y-m-d H:i:s", strtotime($_POST['eventDate']));
        $newEDate = date_format($_POST['eventDate'],"Y-m-d H:i:s");
        $newEAcad = $_POST['eventAcadInst'];
        $newEStatus = $_POST['eventStatus'];
        $newEDesc = $_POST['eventDesc'];
        $newPostedBy = $_POST['postedBy'];
        $newERStatus = $_POST['reviewStatus'];
        
        if(isset($_POST['moderatedBy'])){
            $newModeratedBy = $_POST['moderatedBy'];
        }
        else{
            $newModeratedBy = 0;
        }
        
        
        $now = new DateTime();

        $curDate = date_format($now, "Y-m-d H:i:s");

        //CHECKS IF THE DATE IS BEFORE CURRENT DATE, IN CASE USER FORGETS TO CHANGE THE STATUS FROM OVER TO UPCOMING 
        if (($newEDate > $curDate) && ($newEStatus != 'Cancelled')) {
            $newEStatus = 'Upcoming';
        }
        
        
        $newSqlEName = $this->conn->real_escape_string($newEName);
        $newSqlEDate = $this->conn->real_escape_string($newEDate);
        $newSqlEAcad = $this->conn->real_escape_string($newEAcad);
        $newSqlEStatus = $this->conn->real_escape_string($newEStatus);
        $newSqlEDesc = $this->conn->real_escape_string($newEDesc);
        $newSqlPostedBy = $this->conn->real_escape_string($newPostedBy);
        $newSqlERStatus = $this->conn->real_escape_string($newERStatus);
        $newSqlModeratedBy = $this->conn->real_escape_string($newModeratedBy);

        $sql = "INSERT INTO events (eventName, eventDate,eventAcadInst, eventStatus, eventDesc, postedBy, reviewStatus, moderatedBy) VALUES('$newSqlEName', '$newSqlEDate', '$newSqlEAcad', '$newSqlEStatus', '$newSqlEDesc', '$newSqlPostedBy', '$newSqlERStatus', '$newSqlModeratedBy')";
        echo $sql; 

        $qRes = sqlsrv_query($this->conn, $sql);
        if ($qRes === FALSE) {
            $_SESSION['displayMsg'] = "There has been an error creating event. Please try again." . "Error message" . $this->conn->connect_errno . ": " . $this->conn->connect_error . "\n";
            return false;
        }
        else {
            $_SESSION['displayMsg'] = "<p>Your event has been created succesfully.</p>";
            $_SESSION['displayMsg'] .= "<a href='viewAllEventsUI.php'>Return to Events</a>";
            $_SESSION['displayMsg'] .= "<hr/>";
            $_SESSION['displayMsg'] .= "<br/>Event Name: " . $newEName;
            $_SESSION['displayMsg'] .= "<br/>Event Date: " . $newEDate;
            $_SESSION['displayMsg'] .= "<br/>Event Academic Institution: " . $newEAcad;
            $_SESSION['displayMsg'] .= "<br/>Event Date: " . $newEStatus;
            $_SESSION['displayMsg'] .= "<br/>Event Desc: " . $newEDesc;
            $_SESSION['displayMsg'] .= "<br/>Event Review Status: " . $newERStatus;
            $_SESSION['displayMsg'] .= "<hr/>";
            return true;
        }
    }

    function retrieveEvent($eventID) {

        date_default_timezone_set('Asia/Singapore');

        $now = new DateTime();

        if (isset($eventID)) {
            echo "<h3 style='text-align:center;'>Manage Event</h3>";
            echo "<div class='col pull-right' style='padding-right:20px;'>";
            echo "<a href='viewAllEventsUI.php' class='previous'>&laquo; Return to Events</a>";

            echo "</div>";
            echo "<br/>";
            echo "<hr/>";

            $eventID = (int) $eventID;
            $sql = "SELECT * , FORMAT(eventDate, '%Y-%m-%dT%H:%i') FROM events WHERE eventID = " . $eventID;
            $qRes = sqlsrv_query($this->conn, $sql);

            echo "<form action='manageEventUI.php' method='POST' style='padding-left: 30px; padding-right:30px;'>";

            while ($row = sqlsrv_fetch_array($qRes)) {

                echo "<div class='form-group'>";
                echo "<label for='eventID'>Event ID:</label>";
                echo "<input name='eventID' type='number' class='form-control' value='" . $eventID . "' readonly></input>";
                echo "</div>";

                echo "<div class='form-group'>";
                echo "<label for='eventName'>Event Name:</label>";
                echo "<input name='eventName' type='text' class='form-control' value='" . $row['eventName'] . "' required>";
                echo "</div>";


                //$eventDate = date('Y-m-d\TH:i:s', strtotime($row['eventDate']));
                $eventDate = date_format($row['eventDate'],'Y-m-d\TH:i:s');
                echo "<div class='form-group'>";
                echo "<label for='eventDate'>Event Date:</label>";
                echo "<input name='eventDate' type='datetime-local' class='form-control' value='" . $eventDate . "' min=" . date_format($now, 'Y-m-d') . "T00:00 required>";
                echo "</div>";

                if ($_SESSION['userType'] == 'SIMPartner') {
                    echo "<div class='form-group'>";
                    echo "<label for='eventAcadInst'>Event Academic Institution:</label>";
                    echo "<select class='form-control' name='eventAcadInst'>";
                    //WILL MODIFY AFTER SIM PARTNER REGISTRATION PAGE IS DONE
                    echo "<option value='" . $_SESSION['AwardingInstitute'] . "' selected>" . $_SESSION['AwardingInstitute'] . "</option>";
                    echo "</select>";
                    echo "</div>";
                }
                elseif ($_SESSION['userType'] == 'SIMStaff') {
                    echo "<div class='form-group'>";
                    echo "<label for='eventAcadInst'>Event Academic Institution:</label>";
                    echo "<select class='form-control' name='eventAcadInst'>";

                    $listUnis = array("RMIT University", "University at Buffalo", "University of Stirling", "La Trobe University", "University of Wollongong", "The University of Sydney", "University of Birmingham", "University of London", "SIM Global Education", "The University of Warwick", "Monash College", "Grenoble Ecole de Management");

                    switch ($row['eventAcadInst']) {
                        case "RMIT University":
                            echo "<option value='RMIT University' selected>RMIT University</option>";
                            $ogUni = "RMIT University";
                            break;
                        case "University at Buffalo":
                            echo "<option value='University at Buffalo' selected>University at Buffalo</option>";
                            $ogUni = "University at Buffalo";
                            break;
                        case "University of Stirling":
                            echo "<option value='University at Stirling' selected>University at Stirling</option>";
                            $ogUni = "University at Stirling";
                            break;
                        case "La Trobe University":
                            echo "<option value='La Trobe University' selected>La Trobe University</option>";
                            $ogUni = "La Trobe University";
                            break;
                        case "University of Wollongong":
                            echo "<option value='University of Wollongong' selected>University of Wollongong</option>";
                            $ogUni = "University of Wollongong";
                            break;
                        case "The University of Sydney":
                            echo "<option value='The University of Sydney' selected>The University of Sydney</option>";
                            $ogUni = "The University of Sydney";
                            break;
                        case "University of Birmingham":
                            echo "<option value='University of Birmingham' selected>University of Birmingham</option>";
                            $ogUni = "University of Birmingham";
                            break;
                        case "University of London":
                            echo "<option value='University of London' selected>University of London</option>";
                            $ogUni = "University of London";
                            break;
                        case "SIM Global Education":
                            echo "<option value='SIM Global Education' selected>SIM Global Education</option>";
                            $ogUni = "SIM Global Education";
                            break;
                        case "The University of Warwick":
                            echo "<option value='The University of Warwick' selected>The University of Warwick</option>";
                            $ogUni = "The University of Warwick";
                            break;
                        case "Monash College":
                            echo "<option value='Monash College' selected>Monash College</option>";
                            $ogUni = "Monash College";
                            break;
                        case "Grenoble Ecole de Management":
                            echo "<option value='Grenoble Ecole de Management' selected>Grenoble Ecole de Management</option>";
                            $ogUni = "Grenoble Ecole de Management";
                            break;
                    }

                    foreach ($listUnis as $uni) {
                        if ($uni != $ogUni) {
                            echo "<option value='" . $uni . "'>" . $uni . "</option>";
                        }
                    }

                    echo "</select>";
                    echo "</div>";
                }

                echo "<div class='form-group'>";
                echo "<label for='eventStatus'>Event Status:</label>";
                echo "<select class='form-control' name='eventStatus'>";
                switch ($row['eventStatus']) {
                    case "Upcoming":
                        echo "<option value='Upcoming' selected>Upcoming</option>";
                        echo "<option value='Over'>Over</option>";
                        echo "<option value='Cancelled'>Cancelled</option>";
                        break;
                    case "Over":
                        echo "<option value='Upcoming'>Upcoming</option>";
                        echo "<option value='Over' selected>Over</option>";
                        echo "<option value='Cancelled'>Cancelled</option>";
                        break;
                    case "Cancelled":
                        echo "<option value='Upcoming'>Upcoming</option>";
                        echo "<option value='Over'>Over</option>";
                        echo "<option value='Cancelled' selected>Cancelled</option>";
                        break;
                }
                echo "</select>";
                echo "</div>";

                echo "<div class='form-group'>";
                echo "<label for='eventDesc'>Event Desc:</label>";
                echo "<textarea name='eventDesc' name='paragraph_text' cols='50' rows='10' class='form-control' required>" . $row['eventDesc'] . "</textarea>";
                echo "</div>";
                
                if($_SESSION['userType'] == 'SIMStaff'){
                    echo "<input name='reviewStatus' type='text' value='Approved' hidden>";
                    echo "<input name='moderatedBy' type='text' value='".$_SESSION['userID']."' hidden>";
                }
                elseif($_SESSION['userType'] == 'SIMPartner'){
                    echo "<input name='reviewStatus' type='text' value='Pending' hidden>";
                }

                echo "<div class='text-center'>";
                echo "<button name='saveBut' type='submit' class='btn btn-success'>Save Changes</button>";
                echo "     ";

                echo "<button name='removeBut' type='submit' class='btn btn-danger'>Remove Event</button>";
                echo "</div>";

                echo '</form>';
            }
        }
    }

    function saveChanges() {

        $eventID = $_POST['eventID'];
        $upEName = $_POST['eventName'];
        $upEDate = date_format($_POST['eventDate'], "Y-m-d H:i:s");
        $upEStatus = $_POST['eventStatus'];
        $upEDesc = $_POST['eventDesc'];
        $upERStatus = $_POST['reviewStatus'];
        
        if(isset($_POST['moderatedBy'])){
            $upModeratedBy = $_POST['moderatedBy'];
        }
        else{
            $upModeratedBy = 0;
        }

        $now = new DateTime(); 

        $curDate = date_format($now, "Y-m-d H:i:s");

        //CHECKS IF THE DATE IS BEFORE CURRENT DATE, IN CASE USER FORGETS TO CHANGE THE STATUS FROM OVER TO UPCOMING 
        if (($upEDate > $curDate) && ($upEStatus != 'Cancelled')) {
            $upEStatus = 'Upcoming';
        }

        $sqlEName = $this->conn->real_escape_string($upEName);
        $sqlEDate = $this->conn->real_escape_string($upEDate);
        $sqlEStatus = $this->conn->real_escape_string($upEStatus);
        $sqlEDesc = $this->conn->real_escape_string($upEDesc);
        $sqlERStatus = $this->conn->real_escape_string($upERStatus);
        $sqlModeratedBy = $this->conn->real_escape_string($upModeratedBy);



        $sql = "UPDATE events SET eventName='" . $sqlEName . "',eventDate='" . $sqlEDate . "',eventStatus='" . $sqlEStatus . "',eventDesc='" . $sqlEDesc . "' , reviewStatus='" . $sqlERStatus . "', moderatedBy='".$sqlModeratedBy." WHERE eventID=" . $eventID;


        $qRes = sqlsrv_query($this->conn, $sql);

        if ($qRes == FALSE) {
            $_SESSION['displayMsg'] = "There has been an error saving your changes, please try again." . "Error message" . $this->conn->connect_errno . ": " . $this->conn->connect_error . "\n";
        }
        else {
            $_SESSION['displayMsg'] = "<p>Your changes have been saved succesfully.</p>";
            $_SESSION['displayMsg'] .= "<a href='viewAllEventsUI.php'>Return to Events</a>";
            $_SESSION['displayMsg'] .= "<hr/>";
            $_SESSION['displayMsg'] .= "Event ID: " . $eventID;
            $_SESSION['displayMsg'] .= "<br/>Event Name: " . $upEName;
            $_SESSION['displayMsg'] .= "<br/>Event Date: " . $upEDate;
            $_SESSION['displayMsg'] .= "<br/>Event Status: " . $upEStatus;
            $_SESSION['displayMsg'] .= "<br/>Event Desc: " . $upEDesc;
            $_SESSION['displayMsg'] .= "<br/>Event Review Status: " . $upERStatus;
            $_SESSION['displayMsg'] .= "<hr/>";
        }
    }

    function removeEvent($eventID) {
        $eventID = $_POST['eventID'];

        $sql = "DELETE FROM events WHERE eventID = $eventID";

        //echo $sql;

        $qRes = sqlsrv_query($this->conn, $sql);

        if ($qRes == FALSE) {
            $_SESSION['displayMsg'] = "There has been an error deleting your event, please try again." . "Error message" . $this->conn->connect_errno . ": " . $this->conn->connect_error . "\n";
        }
        else {
            $_SESSION['displayMsg'] = "<p>Your event have been deleted succesfully.</p>";
            $_SESSION['displayMsg'] .= "<a href='viewAllEventsUI.php'>Return to Events</a>";
        }
    }

    function checkEventOver() {
        date_default_timezone_set('Asia/Singapore');
        $now = new DateTime();
        $curDate = strtotime(date_format($now, "Y-m-d G:i:s"));

        $sql = "SELECT * FROM events WHERE eventStatus = 'Upcoming'";
        $qRes = sqlsrv_query($this->conn, $sql);

        while ($row = sqlsrv_fetch_array($qRes)) {
            //print_r($row['eventDate']);
            //echo "<br/>";
            //print_r($curDate);
            //echo "<br/>";

            $retrievedEventDate = strtotime($row['eventDate']);
            
            
            echo date('Y-m-d G:i:s', strtotime($row['eventDate']));
            echo "|";
            //print_r($curDate);
            echo "|";
            if ($retrievedEventDate > $curDate) {
                $upSql = "UPDATE events SET eventStatus='Over' WHERE eventID = " . $row['eventID'];
                $qRes1 = sqlsrv_query($this->conn, $upSql);
                if ($qRes == FALSE) {
                    $_SESSION['displayMsg'] = "There has been an error deleting your event, please try again." . "Error message" . $this->conn->connect_errno . ": " . $this->conn->connect_error . "\n";
                }
            }
        }
    }

    function displayAttendees($eventID) {
        $sql = "SELECT * FROM eventrecords WHERE eventID = '" . $eventID . "'";
        $qRes = sqlsrv_query($this->conn, $sql);

        // TO GET NUMBER OF ATTENDEES
        $sql2 = "SELECT COUNT(eventID) AS attendCount FROM eventrecords WHERE eventID = '" . $eventID . "'";
        $qRes2 = sqlsrv_query($this->conn, $sql2);

        $attendCount = 0;

        echo "<br/>";
        echo "<h3 style='text-align:center;'>" . $_POST['eventName'] . "</h3>";
        echo "<br/>";

        // SQL WHILE LOOP FOR NUMBER OF ATTENDEES 
        while ($row2 = sqlsrv_fetch_array($qRes2)) {
            $attendCount = $row2['attendCount'];
            // DISPLAY FOR ATTENDEE COUNT
            echo "<label align='left' style='padding-left:20px;'><i>Total Of <u style='color:red'>" . $row2['attendCount'] . " Attendees</u></i></label>";
        }
        // END OF SQL WHILE LOOP FOR NUMBER OF ATTENDEES


        echo "<a class='previous pull-right btn btn-link' href='viewAllEventsUI.php' style='padding-right:20px;'>&laquo; Return to Events</a>";
        echo "<br/>";
        echo "<hr/>";


        if ($attendCount == 0) {
            echo "<h4 align='center'>No attendees to display yet.</h4>";
        }
        else {
            echo "<div class='w3-container'>";
            echo "<ul class='w3-ul w3-card-4'>";

            // SQL WHILE LOOP FOR EACH USER BASED ON EVENTSRECORD TABLE
            while ($row = sqlsrv_fetch_array($qRes)) {
                echo "<li class='w3-bar'>";
                echo "<img src='images/img_avatar.png' class='w3-bar-item w3-circle w3-hide-small' style='width:85px'>";
                echo "<div class='w3-bar-item'>";

                $sql3 = "SELECT * FROM userinformation WHERE userID = '" . $row['userID'] . "'";
                $qRes3 = sqlsrv_query($this->conn, $sql3);

                //SQL WHILE LOOP TO DISPLAY USER DETAILS
                while ($row3 = sqlsrv_fetch_array($qRes3)) {
                    echo "<span class='w3-large'>" . $row3['FirstName'] . " " . $row3['LastName'] . "</span><br>";
                    if ($row3['userType'] == 'Student') {
                        echo "<span><i>" . $row3['userType'] . " at " . $row3['AwardingInstitute'] . "</i></span><br/>";
                        echo "<span><i>" . $row3['Qualification'] . "</i></span><br/>";
                    }
                    else {
                        echo "<span>" . $row3['userType'] . "</span><br/>";
                    }
                }
            }
        }
    }

    function displayReview() {

        //date_default_timezone_set('Asia/Singapore');

        echo "<link rel='stylesheet' href='css/eventsCSS.css'>";
        echo "<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>";

        echo "<div class='text-center'>";
        echo "<h3>Review Events</h3>";
        echo "</div>";
        echo "<hr/>";

        echo "<div class='container-fluid d-flex h-100'>";
        if (isset($_SESSION['userType']) && ($_SESSION['userType'] == 'SIMStaff')) {

            $sql = "SELECT * FROM events WHERE reviewStatus = 'Pending'";
            $qRes = sqlsrv_query($this->conn, $sql);

            while ($row = sqlsrv_fetch_array($qRes)) {

                //echo "<div class='space'>";
                echo "</div>";
                echo "<div class='col-sm-12'>";
                echo "<div class='well'>";
                echo "<h4>" . $row['eventName'] . "</h4>";
                echo "<div class='eventContainer'>";


                // START OF CALENDAR WIDGET
                echo "<div class='pull-left' style='padding-right: 20px; padding-left:20px; padding-bottom:10px'>";
                //echo "<time datetime='" . date('d F Y', strtotime($row['eventDate'])) . "' class='icon'>";
                echo "<time datetime='" . date_format($row['eventDate'],'d F Y') . "' class='icon'>";
                //echo "<strong>" . date('F Y', strtotime($row['eventDate'])) . "</strong>";
                echo "<strong>" . date_format($row['eventDate'],'F Y') . "</strong>";
                //echo "<span>" . date('d', strtotime($row['eventDate'])) . "</span>";
                echo "<span>" . date_format($row['eventDate'],'d') . "</span>";
                echo "</time>";
                echo "</div>"; 

                // END OF CALENDAR WIDGET
                //echo "<p class='labels'>" . date('d F Y', strtotime($row['eventDate'])). "</p>";
                echo "<p class='labels'><span><i class='fa fa-clock-o' aria-hidden='true'></i></span> " . date_format($row['eventDate'], 'h:ia') . "</p>";
                //echo "<p class='labels'><span><i class='fa fa-clock-o' aria-hidden='true'></i></span> " . date_format($row['eventDate'], 'Y-m-d H:i:s') . "</p>"; 

                echo " | <strong>" . $row['eventAcadInst'] . "</strong> ";

                if ($row['eventStatus'] == "Upcoming") {
                    echo "|<strong> " . $row['eventStatus'] . "</strong></p>";
                }
                else {
                    echo "<p class='otherStatus'> | " . $row['eventStatus'] . "</p>";
                }


                echo "<p>" . $row['eventDesc'] . "</p>";

                echo "<div class='btn-toolbar'>";
                echo "<form action='reviewEventsUI.php' method='POST'>";

                // APPROVE BUTTON 
                echo "<button name='approveBut[" . $row['eventID'] . "]' type='submit' class='btn btn-success approve' style='margin-right:10px'>Approve Event</button>";
                // END OF APPROVE BUTTON
                // REJECT BUTTON 
                echo "<button name='rejectBut[" . $row['eventID'] . "]' type='submit' class='btn btn-danger reject'>Reject Event</button>";
                // END OF APPROVE BUTTON


                echo "</form>";
                echo "</div>";
                echo "</div>";
                echo "</div>";
                echo "</div>";
            }

            if (sqlsrv_num_rows($qRes) == 0) {
                echo "<h3 style='border-radius: 5px; text-align:center; color:green;'>There are currently no events to be reviewed.</h3>";
                echo "<div align='center'>";
                echo "<a href='viewAllEventsUI.php' class='btn btn-info'>Return to Events</a>";
                echo "</div>";
            }
        }
        elseif (($_SESSION['userType'] == 'SIMPartner')) {
            $sql = 'SELECT * FROM events WHERE postedBy = "'.$_SESSION['userID'].'" AND reviewStatus <> "Approved"';
            $qRes = sqlsrv_query($this->conn, $sql);

            while ($row = sqlsrv_fetch_array($qRes)) {

                //echo "<div class='space'>";
                echo "</div>";
                echo "<div class='col-sm-12'>";
                echo "<div class='well'>";
                echo "<h4>" . $row['eventName'] . "</h4>";
                echo "<div class='eventContainer'>";


                // START OF CALENDAR WIDGET
                echo "<div class='pull-left' style='padding-right: 20px; padding-left:20px; padding-bottom:10px'>";
                //echo "<time datetime='" . date('d F Y', strtotime($row['eventDate'])) . "' class='icon'>";
                echo "<time datetime='" . date_format($row['eventDate'],'d F Y') . "' class='icon'>";
                //echo "<strong>" . date('F Y', strtotime($row['eventDate'])) . "</strong>";
                echo "<strong>" . date_format($row['eventDate'],'F Y') . "</strong>";
                //echo "<span>" . date('d', strtotime($row['eventDate'])) . "</span>";
                echo "<span>" . date_format($row['eventDate'],'d') . "</span>";
                echo "</time>";
                echo "</div>"; 

                // END OF CALENDAR WIDGET
                //echo "<p class='labels'>" . date('d F Y', strtotime($row['eventDate'])). "</p>";
                //echo "<p class='labels'><span><i class='fa fa-clock-o' aria-hidden='true'></i></span> " . date_format($row['eventDate'], 'h:ia') . "</p>";
                //echo "<p class='labels'><span><i class='fa fa-clock-o' aria-hidden='true'></i></span> " . date_format($row['eventDate'], 'Y-m-d H:i:s') . "</p>"; 
                
                if($row['reviewStatus'] == 'Pending'){
                    echo "<strong>Review Status: </strong><label style='background-color:gold; padding:2px; border-radius: 5px'> ".$row['reviewStatus']."</label>";
                }
                elseif($row['reviewStatus'] == 'Rejected'){
                    echo "<strong>Review Status: </strong><label style='background-color:red; padding:2px; border-radius:5px'> ".$row['reviewStatus']."</label>";
                    echo " <label>(<i> ".$row['reviewComment']."</i> )</label>";
                    echo "<strong> | Moderated By: </strong>";
                    
                    //RETRIEVING MODERATOR NAME
                    $sql1 = 'SELECT * FROM userinformation WHERE userID = "'.$row["moderatedBy"].'"';
                    $qRes1 = sqlsrv_query($this->conn, $sql1);

                    while ($row1 = sqlsrv_fetch_array($qRes1)){
                        echo " <label>".$row1['FirstName']. " " . $row1['LastName']."</label>";
                    }
                    //END OF RETRIEVING MODERATOR NAME
                    
                    
                }
//                elseif($row['reviewStatus'] == 'Approved'){
//                    echo "<label style='background-color:lightgreen; padding:2px; border-radius:5px'> ".$row['reviewStatus']."</label></p>";
//                }
                
                echo "<br/>";
                echo "<p class='labels'><span><i class='fa fa-clock-o' aria-hidden='true'></i></span> " . date_format($row['eventDate'],'h:ia') . "</p>";
                echo " | <strong>" . $row['eventAcadInst'] . "</strong> ";

                echo "|<strong> " . $row['eventStatus'] . "</strong> | </p>";
                
                
                echo "<p>" . $row['eventDesc'] . "</p>";

                echo "<div class='btn-toolbar'>";
                echo "<form action='reviewEventsUI.php' method='POST'>";

                echo "<input name='eventName' type='text' value='" . $row['eventName'] . "' hidden >";

                echo "</form>";
                
                // MANAGE EVENT FORM
                echo "<form action='manageEventUI.php' method='POST'>";
                echo "<button name='mgBut[" . $row['eventID'] . "]' type='submit' class='btn btn-info'>Manage Event </button>";
                echo "</form>";
                // END OF MANAGE EVENT FORM
                
                echo "</div>";
                echo "</div>";
                echo "</div>";
                echo "</div>";
                echo "</div>";
                
                
            }

            if (sqlsrv_num_rows($qRes) == 0) {
                echo "<h3 style='border-radius: 5px; text-align: center; color:green;'>All your events have been approved.</h3>";
                echo "<div align='center'>";
                echo "<a href='viewAllEventsUI.php' class='btn btn-info'>Return to Events</a>";
                echo "</div>";
            }
        }
        elseif ((!isset($_SESSION['userType'])) || ($_SESSION['userType'] == 'Student') || ($_SESSION['userType'] == 'Parent') || ($_SESSION['userType'] == 'Parent') || ($_SESSION['userType'] == 'Others')) {
            echo "<div align='center'>";
            echo "<h4 style='color:red; background: none'>Access denied. You do not have permission to view this page.</h4>";
            echo "</div>";
        }
    }

    function updateReviewStatus($eventID, $userID, $status) {

        $eventID = $this->conn->real_escape_string($eventID);
        $status = $this->conn->real_escape_string($status);
        $userID = $this->conn->real_escape_string($userID);

        $sql = "UPDATE events SET reviewStatus='" . $status . "', moderatedBy ='" . $userID . "' WHERE eventID=" . $eventID;

        $qRes = sqlsrv_query($this->conn, $sql);

        if ($qRes == FALSE) {
            $_SESSION['displayMsg'] = "There has been an error saving your changes, please try again." . "Error message" . $this->conn->connect_errno . ": " . $this->conn->connect_error . "\n";
        }
        else {
            $_SESSION['displayMsg'] = "<p>Your changes have been saved succesfully.</p>";
        }
    }

    function __wakeup() {
        include("SIMOpenHouseDB.php");
        $this->conn = $conn;
    }

    function __destruct() {
        if (!$this->conn->connect_error) {
            @$this->conn->close();
        }
    }

}

?>
