<?php session_start(); ?>

<?php
class support {
	private $conn = NULL;
	
    private $email;
	private $name;
	private $msgContent;
    private $answer;
    private $userID;
    private $formType;
    private $status;
	
	function __construct() {
		include ("SIMOpenHouseDB.php");
		$this->conn = $conn;
	}
	
    
	public function setUserID($userID) {
		$this->userID = $userID;
	}
	
	public function getUserID() {
		return $this->userID;
	}
    
    public function setEmail($email) {
		$this->email = $email;
	}
	
	public function getEmail() {
		return $this->email;
	}
    
    public function setName($name) {
		$this->name = $name;
	}
	
	public function getName() {
		return $this->name;
	}
    
    public function setMsgContent($msgContent) {
		$this->msgContent = $msgContent;
	}
	
	public function getMsgContent() {
		return $this->msgContent;
	}
    
    public function setAnswer($answer) {
		$this->answer = $answer;
	}
	
	public function getAnswer() {
		return $this->answer;
	}
    
    public function setFormType($formType) {
		$this->formType = $formType;
	}
	
	public function getFormType() {
		return $this->formType;
	}
    
    public function setStatus($status) {
		$this->status = $status;
	}
	
	public function getStatus() {
		return $this->status;
	}
	
	public function supportForm() {
        
		$this->setName($_SESSION['name']);
        $this->setEmail($_SESSION['email']);
        $this->setMsgContent($_SESSION['comments']);
		$this->setAnswer($_SESSION['answer']);
        $this->setFormType($_SESSION['formType']);
        $this->setStatus($_SESSION['status']);
		
		unset($_SESSION['name']);
		unset($_SESSION['email']);
		unset($_SESSION['comments']);
		unset($_SESSION['answer']);
		unset($_SESSION['formType']);
		unset($_SESSION['status']);
		
		
		$sql = "SELECT * FROM forms";
		
		
		$result = @sqlsrv_query($this->conn, $sql);
		if ($result !== FALSE)
		{
			while (($row = sqlsrv_fetch_array($result)) != FALSE)
			{
				if (
                $this->getName() != $row['name'] ||
                $this->getEmail() != $row['Email'] ||
                $this->getMsgContent() != $row['msgContent'] ||
                $this->getAnswer() != $row['answer'] ||
                $this->getStatus() != $row['status'] ||
                $this->getFormType() != $row['formType'] 
                
                ){
					continue;
				}
				
				else {
				$this->successRedirect();
				}
			}
			
			$sql1 = "INSERT INTO forms" .
					" (name, Email, msgContent, answer, status, formType) " .
					 " VALUES " .
					 " ('".$this->getName()."', '".$this->getEmail()."', '".$this->getMsgContent()."', '".$this->getAnswer()."', '".$this->getStatus()."', '".$this->getFormType()."')";

			$result1 = sqlsrv_query($this->conn, $sql1);

			if ($result1 === FALSE)
			  echo "Unable to execute the query"
					. $this->conn->connect_errno 
					. $this->conn->connect_error;
            
                    
            
			else {
				
				$this->successRedirect();
			}
		}
		
		else {
			echo "Unable to execute the query."
				. $this->conn->connect_errno 
				. $this->conn->connect_error;
		}
		
	}
	
	function successRedirect() {
        ob_start();
        header("refresh:0;url=index.php" );
         echo "<script>alert('Your enquiry has been sent.')</script>";
        ob_end_flush();
        die();
    }
	 
	function failedRedirect() {
        ob_start();
        header( "refresh:0;url=contactUsUI.php" );
        echo "<script>alert('Enquiry Fail')</script>";
        ob_end_flush();
        die();
    }
	
	function __wakeup() {
		include("SIMOpenHouseDb.php");
		$this->conn = $conn;
	}

	function __destruct() {
		if (!$this->conn->connect_error)
			@$this->conn->close();
	}
}
?>

<html>
    <body>
        
        <?php
        
		$support = new support();
     
        $support->supportForm();
			
	?>
	</body>
</html>