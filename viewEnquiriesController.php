<?php

class Enquiry {

    public $conn;

    function __construct() {
        include("SIMOpenHouseDB.php");
        $this->conn = $conn;
    }

    function checkIfFirstMsg($threadID) {
        $sql1 = "SELECT COUNT(threadID) as numOfThread FROM messages WHERE threadID = $threadID";

        $qRes1 = sqlsrv_query($this->conn, $sql1);

        while ($row1 = sqlsrv_fetch_array($qRes1)) {
            if ($row1['numOfThread'] > 1) {
                return "Not First";
            }
            else {
                return "First";
            }
        }
        if (sqlsrv_num_rows($qRes1) == 0) {
            echo "<div align='center'>";
            echo "<h3 style='color:green;'>All messages have been replied to.</h3>";
            echo "<br/>";
            echo "<a class='btn btn-info' style='background-color:grey; border-color:grey;' href='viewAnsweredEnqUI.php'>View Your Answered Messages.</a>";
            echo "</div>";
        }
    }

    function displayEnquiries() {

        $sql = "SELECT * FROM messages WHERE msgStatus = 'Awaiting' AND senderID <> '" . $_SESSION['userID'] . "'";
        $qRes = sqlsrv_query($this->conn, $sql);


        while ($row = sqlsrv_fetch_array($qRes)) {

            $firstMsg = "";
            $firstMsg = $this->checkIfFirstMsg($row['threadID']);

            if ($firstMsg == 'First') {
                echo "<form method='POST' action ='viewConvoUI.php'>";
                echo "<div class='well' style='margin-left:20px; margin-right:20px;'>";
                echo "<label>" . $row['senderName'] . "</label>";
                echo " ";
                //echo "<label> | " . date('d M Y', strtotime($row['sentTime'])) . "</label>";
                echo "<label> | " . date_format($row['sentTime'],'d M Y') . "</label>";
                echo " ";
                //echo "<span>(" . date('h:i a', strtotime($row['sentTime'])) . ")</span>";
                echo "<span>(" . date_format($row['sentTime'],'h:i a') . ")</span>";
//                echo "<input type='text' name='threadID' value='" . $row['threadID'] . "' hidden>";
//                echo "<input type='text' name='senderName' value='" . $row['senderName'] . "' hidden>";
                echo "<br/>";
                echo "<p>" . $row['msgContent'] . "</p>";
                echo "<button name='replyBut[" . $row['threadID'] . "]'>Reply</button>";
                echo "</div>";
                echo "</form>";
                echo "<hr/>";
            }
            if ($firstMsg = '') {
                echo "<div align='center'>";
                echo "<h3 style='color:green;'>All messages have been replied to.</h3>";
                echo "<br/>";
                echo "<a class='btn btn-info' style='background-color:grey; border-color:grey;' href='viewMessagesUI.php'>View Your Answered Messages.</a>";
                echo "</div>";
            }
        }
    }

    function submitReply($rSenderName, $rMsgContent, $ogSenderID, $rThreadID) {
        $senderID = $_SESSION['userID'];
        $msgStatus = 'Awaiting';

        $newRMsgContent = $this->conn->real_escape_string($rMsgContent);

        $sql = "INSERT INTO messages (senderID, senderName, msgContent,threadID, msgStatus) VALUES('$senderID', '$rSenderName', '$newRMsgContent', '$rThreadID', '$msgStatus')";



        $qRes = sqlsrv_query($this->conn, $sql);
        if ($qRes === FALSE) {
            $_SESSION['displayMsg'] = "There has been an error sending your message. Please try again." . "Error message" . $this->conn->connect_errno . ": " . $this->conn->connect_error . "\n";
            return false;
        }
        else {
            $_SESSION['displayMsg'] = "<p>Your message has been sent succesfully.</p>";

            $sql2 = "UPDATE messages SET msgStatus='Replied' WHERE threadID=" . $rThreadID . " AND senderID = $ogSenderID";
            $qRes2 = sqlsrv_query($this->conn, $sql2);
            if ($qRes2 === FALSE) {
                $_SESSION['displayMsg'] = "There has been an error updating. Please try again." . "Error message" . $this->conn->connect_errno . ": " . $this->conn->connect_error . "\n";
            }
            else {
                $_SESSION['displayMsg'] = "<p>Updated succesfully.</p>";
                $_SESSION['reloadConvo'] = "Yes";
                $_SESSION['threadID'] = $rThreadID;
                $_SESSION['fullName'] = $rSenderName;
            }
        }
    }

    function displayUsersMessages($userID, $fullName, $ans) {
        // RETRIEVING THREAD ID INVOLING CURRENT USER
        if ($ans == "Yes") {
            $sql = "SELECT DISTINCT threadID FROM messages WHERE senderID = $userID AND msgStatus = 'Resolved'";
        }
        elseif ($ans == "No") {
            $sql = "SELECT DISTINCT threadID FROM messages WHERE senderID = $userID AND msgStatus <> 'Resolved'";
        }


        $qRes = sqlsrv_query($this->conn, $sql);

        while ($row = sqlsrv_fetch_array($qRes)) {

            // RETRIEVING NAME OF OTHER USER
            $sql1 = "SELECT DISTINCT senderName FROM messages WHERE threadID = '" . $row['threadID'] . "' AND senderName <> '$fullName' ORDER BY messageID";


            $qRes1 = sqlsrv_query($this->conn, $sql1);

            while ($row1 = sqlsrv_fetch_array($qRes1)) {
                //RETRIEVING ALL MESSAGES 
                $sql2 = "SELECT * FROM (SELECT * FROM messages WHERE threadID = " . $row['threadID'] . " ORDER BY messageID DESC LIMIT 2) as results ORDER BY sentTime ASC";

                $qRes2 = sqlsrv_query($this->conn, $sql2);


                echo "<div class='col-sm-12'>";
                echo "<div class='well' style='margin-left:20px; margin-right:20px; padding-bottom: 50px;'>";


                if ($row1['senderName'] != $fullName) {
                    //'CONVERSATION WITH ...'
                    echo "<p style='font-weight:bold; background-color:grey; padding: 20px;'>Conversation with " . $row1['senderName'] . "";
                    echo "<label></label>";
                    echo "</p>";
                }



                while ($row2 = sqlsrv_fetch_array($qRes2)) {
                    echo "<span><pre style='white-space:pre-line;'><b>" . $row2['senderName'] . ":</b> ";
                    echo $row2['msgContent'] . "</pre>";
                    echo "<div class='pull-right'>";
                    echo "<p style='font-size:10px; margin-bottom:0px;'><i>Sent on " . date_format($row2['sentTime'],'d M Y') . " at " . date_format($row2['sentTime'], 'h:i a') . "</i></p>";
                    echo "</div>";
                    echo "<br/>";
                }



                echo "<form role='form' method='POST' action ='viewConvoUI.php' style='float:left; margin-right: 15px; margin-top:5px;'>";
                echo "<button name='viewConvoBut[" . $row['threadID'] . "]' >View Conversation</button>";
                echo "</form>";

                if ($ans == "No") {
                    echo "<form role='form' method='POST' action='viewMessagesUI.php' style='float:left; margin-top:5px;'>";
                    echo "<button name='resolvedBut[" . $row['threadID'] . "]' style='margin-top:0px;' >Resolved</button>";
                    echo "</form>";
                }

                echo "</div>";
                echo "</div>";
            }
        }
    }

    function displayConvo($threadID, $curUser) {
        $senderName = "";

        $sql2 = "SELECT * FROM messages WHERE threadID = " . $threadID . " ORDER BY messageID";

        $qRes2 = sqlsrv_query($this->conn, $sql2);

        $sql3 = "SELECT * FROM messages WHERE threadID = '$threadID' AND senderName <> '$curUser' LIMIT 1";

        $qRes3 = sqlsrv_query($this->conn, $sql3);

        while ($row3 = sqlsrv_fetch_array($qRes3)) {
            echo "<h3 align='center'>Conversation with " . $row3['senderName'] . " </h3>";
            echo "<div class='col pull-right' style='padding-right:20px; padding-bottom:20px;'>";
            echo "<a href='viewMessagesUI.php' class='previous'>&laquo; Return to Inbox</a>";
            echo "</div>";
            echo "<br/>";
            echo "<br/>";

            $recipientID = $row3['senderID'];
        }

        echo "<form role='form' method='POST' action ='viewConvoUI.php'>";
        echo "<div class='well' style='margin-left:20px; margin-right:20px;'>";

        while ($row2 = sqlsrv_fetch_array($qRes2)) {
            if ($row2['senderName'] == $curUser) {
                echo "<div align='right'>";
                echo "<b>" . $row2['senderName'] . ":</b> ";
                $senderName = $row2['senderName'];
                echo "<pre style='white-space:pre-line;'>" . $row2['msgContent'] . "</pre>";
                echo "<p style='font-size:10px; padding-bottom:2px;'><i>Sent on " . date_format($row2['sentTime'],'d M Y') . " at " . date_format($row2['sentTime'], 'h:i a') . "</i></p>";

                echo "</div>";
            }
            else {
                echo "<div align='left'>";
                echo "<b>" . $row2['senderName'] . ":</b> ";
                echo "<pre style='white-space:pre-line;'>" . $row2['msgContent'] . "</pre>";
                echo "<p style='font-size:10px; padding-bottom:2px;'><i>Sent on " . date_format($row2['sentTime'], 'd M Y') . " at " . date_format($row2['sentTime'], 'h:i a') . "</i></p>";
                echo "</div>";
            }
        }
        echo "</div>";

        echo "<div class='form-horizontal' style='margin-left:20px; margin-right:20px;'>";
        echo "<div class='form-group'>";

        echo "<input type='text' name='rThreadID' value='$threadID' hidden>";
        echo "<input type='text' name='recipientID' value='$recipientID' hidden>";

        if ($senderName != "") {
            echo "<input type='text' name='replySenderName' value='$senderName' hidden>";
        }
        else {
            echo "<input type='text' name='replySenderName' value='" . $_SESSION['FirstName'] . " " . $_SESSION['LastName'] . "' hidden>";
        }


        echo "<div class='col-sm-10' style=''>";
        echo "<textarea name='replyMsgContent' rows='3' placeholder='Type your reply here.' style='width:100%;' required></textarea>";
        echo "</div>";

        echo "<div class='col-sm-1' style='padding-left:15px;'>";
        echo "<button style='padding-top: 20px; padding-bottom: 22px; padding-left:20px; padding-right:20px; margin-left:0px;' name='replyFormBut'> Reply </button>";
        echo "</div>";


        echo "</div>";
        echo "</div>";

        echo "</form>";
    }

    function queryResolved($threadID) {
        $sql = "UPDATE messages SET msgStatus='Resolved' WHERE threadID=" . $threadID;

        $qRes = sqlsrv_query($this->conn, $sql);

        if ($qRes === FALSE) {
            $_SESSION['displayMsg'] = "There has been an error signing up, please try again." . "Error message" . $this->conn->connect_errno . ": " . $this->conn->connect_error . "\n";
            return false;
        }
        else {
            $_SESSION['displayMsg'] = "You have successfully signed up!";
            return true;
        }
    }

}
