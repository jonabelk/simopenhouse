<!DOCTYPE html>
<?php
    session_start();
    ob_start();
    include("navbarUI.php");
    include("progController.php");
    
      
    $_SESSION['displayMsg'] = "";
    
    $mgProg = new Programme();
    
     if(isset($_POST['mgProgBut'])){
          $progID = key($_POST['mgProgBut']);
          $_POST['progID'] = $progID;

     }
     else{
         $progID = 0;
     }
    
?>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>
        
        <?php
        
        if(isset($_POST['mgProgBut'])){
            $mgProg->retrieveProg($progID);
        }
        elseif(isset($_POST['addProgBut'])){
            $mgProg->addProgDetails();
        }
         
        
        if(isset($_POST['saveProgBut'])){
//                  $eventID = key($_POST['saveBut']);
//                  $_POST['eventID'] = $eventID;
            //echo $_POST['eventName'];

            $progID = $_POST['progID'];
            $mgProg->saveChanges($progID);
            echo $_SESSION['displayMsg'];
            $_SESSION['displayMsg'] = "";

        }
        
        if(isset($_POST['addProgSqlBut'])){
            $mgProg->addProg();
            echo $_SESSION['displayMsg'];
            $_SESSION['displayMsg'] = "";
        }
        
        if(isset($_POST['removeProgBut'])){
            $progID = $_POST['progID'];
            $mgProg -> removeProg($progID);
            echo $_SESSION['displayMsg'];
            $_SESSION['displayMsg'] = "";
        }
        
        
            
              
//              if($_SESSION['displayMsg'] != ""){
//                  echo "<script> alert('".$_SESSION['displayMsg']."')</script>";
//                  header("Refresh:0");
//                  
//              }
              ?>
        
    </body>
</html>

