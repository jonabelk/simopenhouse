<!DOCTYPE html>
<?php
    session_start();
    ob_start();    
    include("navbarUI.php");
    include("eventsController.php");
    
    $viewAttend = new Event();
    
    if(isset($_POST['viewAttendBut'])){
          $eventID = key($_POST['viewAttendBut']);
          $_POST['eventID'] = $eventID;

     }
     else{
         $eventID = 0;
     }
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>View Attendees</title>
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    </head>
    <body>
           <?php   
           
                if(isset($_POST['viewAttendBut'])){
                    $viewAttend->displayAttendees($eventID);
                }
                
            ?>
          </ul>
        </div>

    </body>
</html>
