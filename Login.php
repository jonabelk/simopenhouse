<?php session_start(); ?>

<?php

class Login {

    private $conn = NULL;
    private $username;
    private $password;

    function __construct() {
        include ("SIMOpenHouseDB.php");
        $this->conn = $conn;

        $this->username = $_SESSION['Email'];

        $this->password = $_SESSION['password'];
    }

    public function loginAUser() {

        $sql = "SELECT * FROM userInformation";


        $result = @sqlsrv_query($this->conn, $sql);
        if ($result !== FALSE) {

            $count = 0;

            while (($row = sqlsrv_fetch_array($result)) != FALSE) {

                if ($this->username == $row['Email'] && md5($this->password) == $row['password_md5']) {
                    $count++;

                    $_SESSION["userID"] = $row['userID'];
                    $_SESSION["userType"] = $row['userType'];
                    $_SESSION["FirstName"] = $row['FirstName'];
                    $_SESSION["LastName"] = $row['LastName'];
                    $_SESSION["Phone"] = $row['Phone'];
                    $_SESSION["Country"] = $row['Country'];
                    $_SESSION["Qualification"] = $row['Qualification'];
                    $_SESSION["AwardingInstitute"] = $row['AwardingInstitute'];
                    $_SESSION["YearOfGrad"] = $row['YearOfGrad'];
                    $_SESSION["password"] = $row['password_md5'];

                    //$_SESSION['username'] = $this->username;
                }
                elseif (($this->username != $row['Email']) && (md5($this->password) != $row['password_md5'])) {


                    $count = 0;
                }
                elseif (($this->username == $row['Email']) && (md5($this->password) != $row['password_md5'])) {

                    $count = 2;
                }
            }

            if ($count == 1) {
                if ($_SESSION["userType"] == 'Parent' || $_SESSION["userType"] == 'Student' || $_SESSION["userType"] == 'Others') {
                    $this->successRedirect1();
                }
                else {
                    $this->successRedirect2();
                }
            }
            elseif ($count == 0) {

                $_SESSION['loginStatus'] = "Invalid";
                unset($_SESSION['Email']);
                unset($_SESSION['password']);
                $this->failedRedirect4();
            }
            elseif ($count == 2) {

                $_SESSION['loginStatus'] = "Invalid";
                unset($_SESSION['Email']);
                unset($_SESSION['password']);
                $this->failedRedirect();
            }
        }
        else {
            echo "Unable to execute the query"
            . $this->conn->connect_errno
            . $this->conn->connect_error;
        }
    }

    function successRedirect1() {
        ob_start();
        header('Location: viewProfile.php');
        ob_end_flush();
        die();
    }

    function successRedirect2() {
        ob_start();
        header('Location: index.php');
        ob_end_flush();
        die();
    }

    function failedRedirect() {
        ob_start();
        //header('refresh:5;url: LoginUI.php');
        header("refresh:0;url=LoginUI.php");
        echo "<script>alert('Please re-enter password.')</script>";
        ob_end_flush();
        die();
    }

    function failedRedirect4() {
        ob_start();
        //header('refresh:5;url: LoginUI.php');
        header("refresh:0;url=LoginUI.php");
        echo "<script>alert('Account not found! Please register.')</script>";
        ob_end_flush();
        die();
    }

    function __wakeup() {
        include("SIMOpenHouseDB.php");
        $this->conn = $conn;
    }

    function __destruct() {
        if (!$this->conn->connect_error)
            @$this->conn->close();
    }

}
?>
<html>
    <body>
<?php
if (isset($_SESSION['Email']) && isset($_SESSION['password'])) {
    $loginUser = new Login();
    $loginUser->loginAUser();
}
else {
    $this->failedRedirect();
}
?>
    </body>
</html>
