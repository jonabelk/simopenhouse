<?php session_start(); ?>

<?php
class Register{
	
    private $userID;
	private $fName;
	private $lName;
	private $phone;
	private $email;
	private $password;
    private $country;
    private $applicantType;
    private $qualification;
    private $awardingInstitute;
    private $yearOfGrad;
    
    
	public function setUserID ($userID) {
		$this->userID = $userID;
	}
	
	public function getUserID () {
		return $this->userID;
	}
    
    public function setYearOfGrad ($yearGrad) {
		$this->yearGrad = $yearGrad;
	}
	
	public function getYearOfGrad () {
		return $this->yearGrad;
	}
	
	public function setfName ($fName) {
		$this->fName = $fName;
	}
	
	public function getfName () {
		return $this->fName;
	}
    
    public function setlName ($lName) {
		$this->lName = $lName;
	}
	
	public function getlName () {
		return $this->lName;
	}
	
	public function setPhone ($phone) {
		$this->phone = $phone;
	}
	
	public function getPhone () {
		return $this->phone;
	}
	
	public function setEmail ($email) {
		$this->email = $email;
	}
	
	public function getEmail () {
		return $this->email;
	}
	
	public function setPassword ($password) {
		$this->password = $password;
	}
	
	public function getPassword () {
		return $this->password;
	}
    
    public function setCountry ($country) {
		$this->country = $country;
	}
	
	public function getCountry() {
		return $this->country;
	}

    
	public function setApplicantType ($applicantType) {
		$this->applicantType = $applicantType;
	}
	
	public function getApplicantType () {
		return $this->applicantType;
	}
    
    public function setQualification ($qualification) {
		$this->qualification = $qualification;
	}
	
	public function getQualification() {
		return $this->qualification;
	}
    
    public function setAwardingInstitute ( $awardingInstitute) {
		$this->awardingInstitute = $awardingInstitute;
	}
	
	public function getAwardingInstitute() {
		return $this->awardingInstitute;
	}    
	
	function successRedirect() {
        ob_start();
        header('refresh:0;url=Register.php');
        
/*        echo $_SESSION['FirstName'];
         
        
        echo $_SESSION['LastName'];
        echo $_SESSION['password'];
        echo $_SESSION['Country'];
        echo $_SESSION['Qualification'];
        echo $_SESSION['AwardingInstitute'];
        echo $_SESSION['YearOfGrad'];
        echo $_SESSION['Email'];
        echo $_SESSION['Phone'];
        echo $_SESSION['userType'];*/
            
            
        ob_end_flush();
        die();
    }
	
	function emptyRedirect() {
        ob_start();
        
        header('Location: RegisterUI.php');
        
        //echo "<script>alert('One or more details not filled')</script>";
        ob_end_flush();
        die();
    }
    
    
    
	public function verifyRegister() {
		if (
            
            ($_POST['FirstName'] != "") &&
            ($_POST['LastName'] != "") &&
            ($_POST['Phone'] != "") &&
            ($_POST['Email'] != "") &&
            ($_POST['password'] != "") && ($_POST['cpassword'] != "") && 
            ($_POST['password']  == $_POST['cpassword']) &&
            ($_POST['Country'] != "")&&
            ($_POST['userType'] != "")&&
            ($_POST['Qualification'] != "")&&($_POST['AwardingInstitute'] != "")&&
            ($_POST['YearOfGrad'] != "")
           )
        
        {
            
            
			//$this->setUserID($_POST['username']);
			$this->setfName($_POST['FirstName']);
            $this->setlName($_POST['LastName']);
			$this->setPhone($_POST['Phone']);
			$this->setEmail($_POST['Email']);
            $this->setCountry($_POST['Country']);
            $this->setApplicantType($_POST['userType']);
            $this->setQualification($_POST['Qualification']);
            $this->setAwardingInstitute($_POST['AwardingInstitute']);
            $this->setYearOfGrad($_POST['YearOfGrad']);
			$this->setPassword($_POST['password']);
            
			$_SESSION['FirstName'] = $this->getfName();
            $_SESSION['LastName'] = $this->getlName();
            $_SESSION['Phone'] = $this->getPhone();
            $_SESSION['Email'] = $this->getEmail();
            $_SESSION['Country'] = $this->getCountry();
            $_SESSION['userType'] = $this->getApplicantType();
            $_SESSION['Qualification'] = $this->getQualification();
            $_SESSION['AwardingInstitute'] = $this->getAwardingInstitute();
            $_SESSION['YearOfGrad'] = $this->getYearofGrad();
            $_SESSION['password'] = $this->getPassword();
			
			$this->successRedirect();
		}
		else {
			$_SESSION['registerStatus'] = 'Half-Half';
			$this->emptyRedirect();
		}
    }
	
}
?>

<html>
    <body>
	<?php 
	
	    $user = new Register();
		
	    if (isset($_POST['submit'])){
            
            
            
		    $user->verifyRegister();
            
           
            
            
		}
		else {
			$user->emptyRedirect();
		}
	?>
	</body>
</html>