<!DOCTYPE html>
<?php
    session_start();
    ob_start();
    
    $_SESSION['displayMsg'] = "";
    
    include("navbarUI.php");
    include("eventsController.php");
   
    $event = new Event();
    
?>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/main.css">
        <title>View Past Events</title>
    </head>
    <body>
        <div class="text-center">
            <h3>Past Events</h3>
            <p>Listing all past events.</p>
        </div>
        <section class='viewAllEvents'>
              <?php $event->displayPastEvents(); ?>
          </section>
          
    </body>
</html>
