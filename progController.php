<?php

class Programme {

    public $conn;

    function __construct() {
        include ("SIMOpenHouseDB.php");
        $this->conn = $conn;
    }

    function displayAllProg() {

        $sql = "SELECT * FROM programmes WHERE progRStatus = 'Approved'";
        $qRes = sqlsrv_query($this->conn, $sql);

        date_default_timezone_set('Asia/Singapore');


//        echo "<div class='eventList'>";
//        echo "<div class='eventItem'>";
//        echo "<div class='well'>";
//        echo "<img src='https://placehold.it/1200x400?text=IMAGE' class='img-circle' height='65' width='55' alt='Avatar'>";
//        echo "<p><br> </p>";
//        echo "</div>";
//        echo "</div>";

        echo "<link rel='stylesheet' href='css/eventsCSS.css'>";
        echo "<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>";

        if (isset($_SESSION['userType'])) {
            if ($_SESSION['userType'] == 'SIMStaff' || $_SESSION['userType'] == 'SIMPartner') {
                // ADD PROGRAMME FORM
                echo "<div class='col-sm-2 pull-right'>";
                echo "<form action='manageProgUI.php' method='POST'>";
                echo "<button type='submit' name='addProgBut' class='btn btn-success'>Add Programme</button>";
                echo "</form>";
                echo "</div>";

                // END OF ADD PROGRAMME FORM
            }
        }

        echo "<div class='container-fluid d-flex h-100'>";

        while ($row = sqlsrv_fetch_array($qRes)) {
            echo "</div>";
            echo "<div class='col-sm-12'>";
            echo "<div class='well'>";
            echo "<h4>" . $row['progName'] . "</h4>";

            echo "<div class='progContainer'>";
            echo "<label class='progLabel'>" . $row['progUni'] . "</label>";
            echo "<p>" . $row['progDesc'] . "</p>";

            if (isset($_SESSION['userType'])) {
                if ($_SESSION['userType'] == 'Student' xor $_SESSION['userType'] == 'Parent' xor $_SESSION['userType'] == 'Others') {

                    //CHECK IF USER HAS ALREADY INDICATED INTEREST, CHANGE BUTTON ACCORDINGLY
                    echo "<form action='viewAllProgUI.php' method='POST'>";
                    $this->checkIndicate($row['progID'], $_SESSION['userID']);
                    echo "</form>";
                }
                elseif ($_SESSION['userType'] == 'SIMStaff' || $_SESSION['userType'] == 'SIMPartner') {

                    echo "<div class='btn-toolbar'>";

                    // VIEW INTERESTED PARTIES FORM
                    echo "<form action='viewInterestedUI.php' method='POST'>";
                    echo "<button name='viewIntBut[" . $row['progID'] . "]' type='submit' class='btn btn-info'>View Interested Parties</button>";
                    echo "</form>";
                    // END OF VIEW INTERESTED PARTIES FORM

                    if ($_SESSION['userType'] == 'SIMPartner') {
                        // WILL MODIFY AFTER SIM PARTNER REGISTRATION DONE
                        if ($_SESSION['AwardingInstitute'] == $row['progUni']) {
                            // WILL MODIFY AFTER SIM PARTNER REGISTRATION PAGE DONE
                            echo "<form action='manageProgUI.php' method='POST'>";
                            echo "<button name='mgProgBut[" . $row['progID'] . "]' type='submit' class='btn btn-info'>Manage Programme </button>";
                            echo "</form>";
                            // END OF MANAGE PROGRAMME FORM
                        }
                    }
                    elseif ($_SESSION['userType'] == 'SIMStaff') {
                        // MANAGE PROGRAMME FORM
                        echo "<form action='manageProgUI.php' method='POST'>";
                        echo "<button name='mgProgBut[" . $row['progID'] . "]' type='submit' class='btn btn-info'>Manage Programme </button>";
                        echo "</form>";
                        // END OF MANAGE PROGRAMME FORM
                    }



                    echo "</div>";
                }
            }
            else {
                // IF USER NOT LOGGED IN, DISPLAY THE FOLLOWING
                echo "<form action='loginUI.php' method='POST'>";
                echo "<button name='login' class='btn btn-info' type='submit'> Login to Indicate Interest </button>";
                echo "</form>";
            }

            echo "</div>";
            echo "</div>";
            echo "</div>";
        }
    }

    function checkIndicate($progID, $userID) {
        $userID = (int) $userID;
        $progID = (int) $progID;
        $sql = "SELECT COUNT(*) as num FROM proginterested WHERE userID = " . $userID . " AND progID = " . $progID;
        $qRes = sqlsrv_query($this->conn, $sql);

        while ($row1 = sqlsrv_fetch_array($qRes)) {
            if ($row1['num'] == 0) {
                echo "<button class='btn btn-info' name='indicateIntBut[" . $progID . "]' type='submit'> Indicate Interest </button>";
            }
            elseif ($row1['num'] > 0) {
                $sql3 = "SELECT * FROM programmes WHERE progID = $progID";

                $qRes3 = sqlsrv_query($this->conn, $sql3);

                while ($row3 = sqlsrv_fetch_array($qRes3)) {
                    echo "<button class='btn btn-danger' name='cancelIndicateBut[" . $progID . "]' type='submit'> Remove Interest </button>";
                }
            }
        }
    }

    function indicateInt($progID, $userID) {
        $sql = "INSERT INTO proginterested (progID, userID) VALUES ($progID, $userID)";

        $qRes = sqlsrv_query($this->conn, $sql);
        if ($qRes === FALSE) {
            $_SESSION['displayMsg'] = "There has been an error indicating your interest, please try again." . "Error message" . $this->conn->connect_errno . ": " . $this->conn->connect_error . "\n";
            return false;
        }
        else {
            $_SESSION['displayMsg'] = "You have successfully indicated your interested!";
            return true;
        }
    }

    function cancelIndicate($progID, $userID) {
        $sql2 = "DELETE FROM proginterested WHERE progID = $progID AND userID = $userID";
        $qRes2 = sqlsrv_query($this->conn, $sql2);

        if ($qRes2 == FALSE) {
            $_SESSION['displayMsg'] = "There has been an error removing your interest in the programme, please try again." . "Error message" . $this->conn->connect_errno . ": " . $this->conn->connect_error . "\n";
            return false;
        }
        else {
            $_SESSION['displayMsg'] = "<p>You have successfully removed your interest in the programme!</p>";
            return true;
        }
    }

    function addProgDetails() {
        date_default_timezone_set('Asia/Singapore');
        //echo date('Y-m-d\TH:i:s');

        echo "<h3 style='text-align:center;'>Add Programme</h3>";
        echo "<div class='col pull-right' style='padding-right:20px;'>";
        echo "<a href='viewAllProgUI.php' class='previous'>&laquo; Return to Programmes</a>";
        echo "</div>";
        echo "<br/><hr/>";
        echo "<form action='manageProgUI.php' method='POST' style='padding-left: 30px; padding-right:30px;'>";

        echo "<div class='form-group'>";
        echo "<label for='progName'>Programme Name:</label>";
        echo "<input name='progName' type='text' class='form-control' value='' required>";
        echo "</div>";

        if ($_SESSION['userType'] == 'SIMPartner') {
            echo "<div class='form-group'>";
            echo "<label for='progUni'>Programme University:</label>";
            echo "<select class='form-control' name='progUni'>";

            //WILL MODIFY AFTER SIM PARTNER REGISTRATION PAGE IS DONE
            echo "<option value='" . $_SESSION['AwardingInstitute'] . "' selected>" . $_SESSION['AwardingInstitute'] . "</option>";
            echo "</select>";
            echo "</div>";
        }
        elseif ($_SESSION['userType'] == 'SIMStaff') {
            echo "<div class='form-group'>";
            echo "<label for='progUni'>Programme University:</label>";
            echo "<select class='form-control' name='progUni'>";

            $listUnis = array("RMIT University", "University at Buffalo", "University of Stirling", "La Trobe University", "University of Wollongong", "The University of Sydney", "University of Birmingham", "University of London", "SIM Global Education", "The University of Warwick", "Monash College", "Grenoble Ecole de Management");

            foreach ($listUnis as $uni) {
                echo "<option value='" . $uni . "'>" . $uni . "</option>";
            }

            echo "</select>";
            echo "</div>";
        }

        echo "<div class='form-group'>";
        echo "<label for='progDesc'>Programme Desc:</label>";
        echo "<textarea name='progDesc' name='paragraph_text' cols='50' rows='10' class='form-control' required></textarea>";
        echo "</div>";

        echo "<div class='form-group'>";
        echo "<input name='progPostedBy' type='text' value='" . $_SESSION['userID'] . "' hidden>";

        if ($_SESSION['userType'] == 'SIMStaff') {
            echo "<input name='progRStatus' type='text' value='Approved' hidden>";
            echo "<input name='progModeratedBy' type='text' value='" . $_SESSION['userID'] . "' hidden>";
        }
        elseif ($_SESSION['userType'] == 'SIMPartner') {
            echo "<input name='progRStatus' type='text' value='Pending' hidden>";
        }

        echo "<div class='text-center'>";
        echo "<button name='addProgSqlBut' type='submit' class='btn btn-success'>Add Programme</button>";
        echo "</div>";

        echo '</form>';
    }

    function addProg() {
        $newPName = $_POST['progName'];
        $newPUni = $_POST['progUni'];
        $newPDesc = $_POST['progDesc'];
        $newPPostedBy = $_POST['progPostedBy'];
        $newPRStatus = $_POST['progRStatus'];

        if (isset($_POST['moderatedBy'])) {
            $newPModeratedBy = $_POST['progModeratedBy'];
        }
        else {
            $newPModeratedBy = 0;
        }

        $newSqlPName = $this->conn->real_escape_string($newPName);
        $newSqlPUni = $this->conn->real_escape_string($newPUni);
        $newSqlPDesc = $this->conn->real_escape_string($newPDesc);
        $newSqlPPostedBy = $this->conn->real_escape_string($newPPostedBy);
        $newSqlPRStatus = $this->conn->real_escape_string($newPRStatus);
        $newSqlPModeratedBy = $this->conn->real_escape_string($newPModeratedBy);

        $sql = "INSERT INTO programmes (progName, progUni, progDesc, progPostedBy, progRStatus, progModeratedBy) VALUES('$newSqlPName', '$newSqlPUni', '$newSqlPDesc', '$newSqlPPostedBy', '$newSqlPRStatus', '$newSqlPModeratedBy')";

        $qRes = sqlsrv_query($this->conn, $sql);
        if ($qRes === FALSE) {
            $_SESSION['displayMsg'] = "There has been an error creating programme. Please try again." . "Error message" . $this->conn->connect_errno . ": " . $this->conn->connect_error . "\n";
            return false;
        }
        else {
            $_SESSION['displayMsg'] = "<p>Your programme has been created succesfully.</p>";
            $_SESSION['displayMsg'] .= "<a href='viewAllProgUI.php'>Return to Programmes</a>";
            $_SESSION['displayMsg'] .= "<hr/>";
            $_SESSION['displayMsg'] .= "<br/><b>Programme Name: </b>" . $newPName;
            $_SESSION['displayMsg'] .= "<br/><b>Programme University: </b>" . $newPUni;
            $_SESSION['displayMsg'] .= "<br/><b>Programme Desc: </b>" . $newPDesc;
            $_SESSION['displayMsg'] .= "<br/><b>Programme Review Status: </b>" . $newSqlPRStatus;
            $_SESSION['displayMsg'] .= "<hr/>";
            return true;
        }
    }

    function retrieveProg($progID) {

        date_default_timezone_set('Asia/Singapore');

        if (isset($progID)) {
            echo "<h3 style='text-align:center;'>Manage Programme</h3>";
            echo "<div class='col pull-right' style='padding-right:20px;'>";
            echo "<a href='viewAllProgUI.php' class='previous'>&laquo; Return to Programmes</a>";

            echo "</div>";
            echo "<br/>";
            echo "<hr/>";

            $progID = (int) $progID;
            $sql = "SELECT * FROM programmes WHERE progID = " . $progID;
            $qRes = sqlsrv_query($this->conn, $sql);

            echo "<form action='manageProgUI.php' method='POST' style='padding-left: 30px; padding-right:30px;'>";

            while ($row = sqlsrv_fetch_array($qRes)) {

                echo "<div class='form-group'>";
                echo "<label for='progID'>Programme ID:</label>";
                echo "<input name='progID' type='number' class='form-control' value='" . $progID . "' readonly></input>";
                echo "</div>";

                echo "<div class='form-group'>";
                echo "<label for='progName'>Programme Name:</label>";
                echo "<input name='progName' type='text' class='form-control' value='" . $row['progName'] . "'>";
                echo "</div>";


                if ($_SESSION['userType'] == 'SIMPartner') {
                    echo "<div class='form-group'>";
                    echo "<label for='progUni'>Programme University:</label>";
                    echo "<select class='form-control' name='progUni'>";
                    //WILL MODIFY AFTER SIM PARTNER REGISTRATION PAGE IS DONE
                    echo "<option value='" . $_SESSION['AwardingInstitute'] . "' selected>" . $_SESSION['AwardingInstitute'] . "</option>";
                    echo "</select>";
                    echo "</div>";
                }
                elseif ($_SESSION['userType'] == 'SIMStaff') {
                    echo "<div class='form-group'>";
                    echo "<label for='progUni'>Programme University:</label>";
                    echo "<select class='form-control' name='progUni'>";

                    $listUnis = array("RMIT University", "University at Buffalo", "University of Stirling", "La Trobe University", "University of Wollongong", "The University of Sydney", "University of Birmingham", "University of London", "SIM Global Education", "The University of Warwick", "Monash College", "Grenoble Ecole de Management");

                    switch ($row['progUni']) {
                        case "RMIT University":
                            echo "<option value='RMIT University' selected>RMIT University</option>";
                            $ogUni = "RMIT University";
                            break;
                        case "University at Buffalo":
                            echo "<option value='University at Buffalo' selected>University at Buffalo</option>";
                            $ogUni = "University at Buffalo";
                            break;
                        case "University of Stirling":
                            echo "<option value='University at Stirling' selected>University at Stirling</option>";
                            $ogUni = "University at Stirling";
                            break;
                        case "La Trobe University":
                            echo "<option value='La Trobe University' selected>La Trobe University</option>";
                            $ogUni = "La Trobe University";
                            break;
                        case "University of Wollongong":
                            echo "<option value='University of Wollongong' selected>University of Wollongong</option>";
                            $ogUni = "University of Wollongong";
                            break;
                        case "The University of Sydney":
                            echo "<option value='The University of Sydney' selected>The University of Sydney</option>";
                            $ogUni = "The University of Sydney";
                            break;
                        case "University of Birmingham":
                            echo "<option value='University of Birmingham' selected>University of Birmingham</option>";
                            $ogUni = "University of Birmingham";
                            break;
                        case "University of London":
                            echo "<option value='University of London' selected>University of London</option>";
                            $ogUni = "University of London";
                            break;
                        case "SIM Global Education":
                            echo "<option value='SIM Global Education' selected>SIM Global Education</option>";
                            $ogUni = "SIM Global Education";
                            break;
                        case "The University of Warwick":
                            echo "<option value='The University of Warwick' selected>The University of Warwick</option>";
                            $ogUni = "The University of Warwick";
                            break;
                        case "Monash College":
                            echo "<option value='Monash College' selected>Monash College</option>";
                            $ogUni = "Monash College";
                            break;
                        case "Grenoble Ecole de Management":
                            echo "<option value='Grenoble Ecole de Management' selected>Grenoble Ecole de Management</option>";
                            $ogUni = "Grenoble Ecole de Management";
                            break;
                    }


                    foreach ($listUnis as $uni) {
                        if ($uni != $ogUni) {
                            echo "<option value='" . $uni . "'>" . $uni . "</option>";
                        }
                    }

                    echo "</select>";
                    echo "</div>";
                }



                echo "<div class='form-group'>";
                echo "<label for='progDesc'>Programme Description:</label>";
                echo "<textarea name='progDesc' name='paragraph_text' cols='50' rows='10' class='form-control'>" . $row['progDesc'] . "</textarea>";
                echo "</div>";

                echo "<div class='form-group'>";
                echo "<input name='progPostedBy' type='text' value='" . $_SESSION['userID'] . "' hidden>";

                if ($_SESSION['userType'] == 'SIMStaff') {
                    echo "<input name='progRStatus' type='text' value='Approved' hidden>";
                    echo "<input name='progModeratedBy' type='text' value='" . $_SESSION['userID'] . "' hidden>";
                }
                elseif ($_SESSION['userType'] == 'SIMPartner') {
                    echo "<input name='progRStatus' type='text' value='Pending' hidden>";
                }

                echo "<div class='text-center'>";
                echo "<button name='saveProgBut' type='submit' class='btn btn-success'>Save Changes</button>";
                echo "     ";

                echo "<button name='removeProgBut' type='submit' class='btn btn-danger'>Remove Programme</button>";
                echo "</div>";

                echo '</form>';
            }
        }
    }

    function saveChanges() {
        $progID = $_POST['progID'];
        $upPName = $_POST['progName'];
        $upPUni = $_POST['progUni'];
        $upPDesc = $_POST['progDesc'];
        $upPRStatus = $_POST['progRStatus'];
        
        if(isset($_POST['progModeratedBy'])){
            $upPModeratedBy = $_POST['progModeratedBy'];
        }
        else{
            $upPModeratedBy = 0;
        }

        $sqlPName = $this->conn->real_escape_string($upPName);
        $sqlPUni = $this->conn->real_escape_string($upPUni);
        $sqlPDesc = $this->conn->real_escape_string($upPDesc);
        $sqlPRStatus = $this->conn->real_escape_string($upPRStatus);
        $sqlPModeratedBy = $this->conn->real_escape_string($upPModeratedBy);

        $sql = "UPDATE programmes SET progName='" . $sqlPName . "',progUni='" . $sqlPUni . "',progDesc='" . $sqlPDesc . "', progRStatus='".$sqlPRStatus."', progModeratedbY='".$sqlPModeratedBy."' WHERE progID=" . $progID;

        //echo $sql;

        $qRes = sqlsrv_query($this->conn, $sql);

        if ($qRes == FALSE) {
            $_SESSION['displayMsg'] = "There has been an error saving your changes, please try again." . "Error message" . $this->conn->connect_errno . ": " . $this->conn->connect_error . "\n";
        }
        else {
            $_SESSION['displayMsg'] = "<p>Your changes have been saved succesfully.</p>";
            $_SESSION['displayMsg'] .= "<a href='viewAllProgUI.php'>Return to Programmes</a>";
            $_SESSION['displayMsg'] .= "<hr/>";
            $_SESSION['displayMsg'] .= "<b>Programme ID: </b>" . $progID;
            $_SESSION['displayMsg'] .= "<br/><b>Programme Name: </b> " . $upPName;
            $_SESSION['displayMsg'] .= "<br/><b>Programme Uni: </b>" . $upPUni;
            $_SESSION['displayMsg'] .= "<br/><b>Programme Desc: </b>" . $upPDesc;
            $_SESSION['displayMsg'] .= "<br/><b>Programme Review Status: </b>" . $upPRStatus;
            $_SESSION['displayMsg'] .= "<hr/>";
        }
    }

    function removeProg($progID) {
        $progID = $_POST['progID'];

        $sql = "DELETE FROM programmes WHERE progID = $progID";

        $qRes = sqlsrv_query($this->conn, $sql);

        if ($qRes == FALSE) {
            $_SESSION['displayMsg'] = "There has been an error deleting your programme, please try again." . "Error message" . $this->conn->connect_errno . ": " . $this->conn->connect_error . "\n";
        }
        else {
            $_SESSION['displayMsg'] = "<p>Your programme have been deleted succesfully.</p>";
            $_SESSION['displayMsg'] .= "<a href='viewAllProgUI.php'>View All Programmes</a>";
        }
    }

    function displayInterest($progID) {
        $sql = "SELECT * FROM proginterested WHERE progID = '" . $progID . "'";
        $qRes = sqlsrv_query($this->conn, $sql);


        $sql2 = "SELECT COUNT($progID) AS progCount FROM proginterested WHERE progID = '" . $progID . "'";
        $qRes2 = sqlsrv_query($this->conn, $sql2);

        while ($row2 = sqlsrv_fetch_array($qRes2)) {
            echo "<br/>";
            echo "<label align='left' style='padding-left:20px;'><i>Total Of <u style='color:red'>" . $row2['progCount'] . " Interested Parties</u></i></label>";
            echo "<a class='btn btn-link pull-right' href='viewAllProgUI.php' style='padding-right:20px;'>Return to Programmes</a>";
            echo "<div class='w3-container'>";
            echo "<ul class='w3-ul w3-card-4'>";
        }

        while ($row = sqlsrv_fetch_array($qRes)) {
            echo "<li class='w3-bar'>";
            echo "<img src='images/img_avatar.png' class='w3-bar-item w3-circle w3-hide-small' style='width:85px'>";
            echo "<div class='w3-bar-item'>";

            $sql3 = "SELECT * FROM userinformation WHERE userID = '" . $row['userID'] . "'";
            $qRes3 = sqlsrv_query($this->conn, $sql3);

            while ($row3 = sqlsrv_fetch_array($qRes3)) {
                echo "<span class='w3-large'>" . $row3['FirstName'] . " " . $row3['LastName'] . "</span><br/>";


                if ($row3['userType'] == 'Student') {
                    echo "<span><i>" . $row3['userType'] . " at " . $row3['AwardingInstitute'] . "</i></span><br/>";
                    echo "<span><i>" . $row3['Qualification'] . "</i></span><br/>";
                }
                else {
                    echo "<span>" . $row3['userType'] . "</span><br/>";
                }

                echo "</div>";
                echo "</li>";
            }
        }
    }

    function displayReviewProg() {
        //date_default_timezone_set('Asia/Singapore');

        echo "<link rel='stylesheet' href='css/eventsCSS.css'>";
        echo "<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>";

        echo "<div class='text-center'>";
        echo "<h3>Review Programmes</h3>";
        echo "</div>";
        echo "<hr/>";

        echo "<div class='container-fluid d-flex h-100'>";
        if (isset($_SESSION['userType']) && ($_SESSION['userType'] == 'SIMStaff')) {

            $sql = "SELECT * FROM programmes WHERE progRStatus = 'Pending'";
            $qRes = sqlsrv_query($this->conn, $sql);

            while ($row = sqlsrv_fetch_array($qRes)) {

                //echo "<div class='space'>";
                echo "</div>";
                echo "<div class='col-sm-12'>";
                echo "<div class='well'>";
                echo "<h4>" . $row['progName'] . "</h4>";
                echo "<div class='progContainer'>";
                echo "<label class='progLabel'>" . $row['progUni'] . "</label>";
                echo "<p>" . $row['progDesc'] . "</p>";

                echo "<div class='btn-toolbar'>";
                echo "<form action='reviewProgUI.php' method='POST'>";

                // APPROVE BUTTON 
                echo "<button name='approveBut[" . $row['progID'] . "]' type='submit' class='btn btn-success approve' style='margin-right:10px'>Approve Programme</button>";
                // END OF APPROVE BUTTON
                // REJECT BUTTON 
                echo "<button name='rejectBut[" . $row['progID'] . "]' type='submit' class='btn btn-danger reject'>Reject Programme</button>";
                // END OF APPROVE BUTTON


                echo "</form>";
                echo "</div>";
                echo "</div>";
                echo "</div>";
                echo "</div>";
            }

            if (sqlsrv_num_rows($qRes) == 0) {
                echo "<h3 style='border-radius: 5px; text-align:center; color:green;'>There are currently no programmes to be reviewed.</h3>";
                echo "<div align='center'>";
                echo "<a href='viewAllProgUI.php' class='btn btn-info'>Return to Programmes</a>";
                echo "</div>";
            }
        }
        elseif (($_SESSION['userType'] == 'SIMPartner')) {
            $sql = 'SELECT * FROM programmes WHERE progPostedBy = "' . $_SESSION['userID'] . '" AND progRStatus <> "Approved"';
            $qRes = sqlsrv_query($this->conn, $sql);

            while ($row = sqlsrv_fetch_array($qRes)) {

                //echo "<div class='space'>";
                echo "</div>";
                echo "<div class='col-sm-12'>";
                echo "<div class='well'>";
                echo "<h4>" . $row['progName'] . "</h4>";
                echo "<div class='progContainer'>";
                echo "<label class='progLabel'>" . $row['progUni'] . "</label>";
                echo " | ";



                if ($row['progRStatus'] == 'Pending') {
                    echo "<strong>Review Status: </strong><label style='background-color:gold; padding:2px; border-radius: 5px'> " . $row['progRStatus'] . "</label>";
                }
                elseif ($row['progRStatus'] == 'Rejected') {
                    echo "<strong>Review Status: </strong><label style='background-color:red; padding:2px; border-radius:5px'> " . $row['progRStatus'] . "</label>";
                    echo "<strong> | Moderated By: </strong>";

                    //RETRIEVING MODERATOR NAME
                    $sql1 = 'SELECT * FROM userinformation WHERE userID = "' . $row["progModeratedBy"] . '"';
                    $qRes1 = sqlsrv_query($this->conn, $sql1);

                    while ($row1 = sqlsrv_fetch_array($qRes1)) {
                        echo " <label>" . $row1['FirstName'] . " " . $row1['LastName'] . "</label>";
                    }
                    //END OF RETRIEVING MODERATOR NAME
                }


                echo "<br/>";
                echo "<p>" . $row['progDesc'] . "</p>";


                echo "<div class='btn-toolbar'>";
                echo "<form action='reviewProgUI.php' method='POST'>";

                echo "<input name='progName' type='text' value='" . $row['progName'] . "' hidden >";

                echo "</form>";

                // MANAGE PROG FORM
                echo "<form action='manageProgUI.php' method='POST'>";
                echo "<button name='mgProgBut[" . $row['progID'] . "]' type='submit' class='btn btn-info'>Manage Programme </button>";
                echo "</form>";
                // END OF MANAGE PROG FORM

                echo "</div>";
                echo "</div>";
                echo "</div>";
                echo "</div>";
                echo "</div>";
            }

            if (sqlsrv_num_rows($qRes) == 0) {
                echo "<h3 style='border-radius: 5px; text-align: center; color:green;'>All your programmes have been approved.</h3>";
                echo "<div align='center'>";
                echo "<a href='viewAllProgUI.php' class='btn btn-info'>Return to Programmes</a>";
                echo "</div>";
            }
        }
        elseif ((!isset($_SESSION['userType'])) || ($_SESSION['userType'] == 'Student') || ($_SESSION['userType'] == 'Parent') || ($_SESSION['userType'] == 'Parent') || ($_SESSION['userType'] == 'Others')) {
            echo "<div align='center'>";
            echo "<h4 style='color:red; background: none'>Access denied. You do not have permission to view this page.</h4>";
            echo "</div>";
        }
    }

    function updateProgReviewStatus($progID, $userID, $status) {

        $eventID = $this->conn->real_escape_string($progID);
        $status = $this->conn->real_escape_string($status);
        $userID = $this->conn->real_escape_string($userID);

        $sql = "UPDATE programmes SET progRStatus='" . $status . "', progModeratedBy ='" . $userID . "' WHERE progID=" . $progID;

        $qRes = sqlsrv_query($this->conn, $sql);

        if ($qRes == FALSE) {
            $_SESSION['displayMsg'] = "There has been an error saving your changes, please try again." . "Error message" . $this->conn->connect_errno . ": " . $this->conn->connect_error . "\n";
        }
        else {
            $_SESSION['displayMsg'] = "<p>Your changes have been saved succesfully.</p>";
        }
    }

    function __wakeup() {
        include("SIMOpenHouseDB.php");
        $this->conn = $conn;
    }

    function __destruct() {
        if (!$this->conn->connect_error) {
            @$this->conn->close();
        }
    }

}

?>
