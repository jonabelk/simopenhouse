<?php

include ('navbarUI.php');

class AddStudentLifeUI {
	public function displayUI() {
		return "
<div class='container'>
  <h2 class='text-center'>Upload Student Life</h2>
  
  <form class='form-horizontal' action='AddStudentLifeController.php' method='post'>
    <div class='form-group'>
      <label class='control-label col-sm-2' for='Title'>Title:</label>
      <div class='col-sm-10'>
        <input type='text' class='form-control' placeholder='Career Development' name='title'>
      </div>
    </div>
    
    <!-- link -->
    <div class='form-group'>
      <label class='control-label col-sm-2' for='link'>Link:</label>
      <div class='col-sm-10'>          
        <input type='text' class='form-control' placeholder='www.sim.edu.sg' name='link'>
      </div>
    </div>
    
        <div class='form-group'>
      <label class='control-label col-sm-2' for='image'>Upload Image:</label>
      <div class='col-sm-10'>          
        <input type='file' class='form-control' placeholder='Upload' name='image'>
      </div>
    </div>
    
    </div>
    <div class='form-group'>        
      <div class='col-sm-offset-2 col-sm-10'>
        <button type='submit' class='btn btn-default' name='submit' value='submit'>Upload</button>
      </div>
    </div>
  </form>
</div>

            ";
	}
}

?>
<html>

  <head>
    <title>Student Life</title>
    <meta http-equiv='content-type' content='text/html; charset=iso-8859-1' />
    <link href='//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css' rel='stylesheet' id='bootstrap-css'>
    <script src='//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js'></script>
    <script src='//code.jquery.com/jquery-1.11.1.min.js'></script>
     
    
  </head>

  <body>
    <?php
                
		$UI = new AddStudentLifeUI();
		echo $UI->displayUI();
        
	?>


  </body>

</html>
