<?php
include ('navbarUI.php');
class LoginUI {
	public function displayLogin() {
		return
            
    "<div class='container'>
  <h2 class='text-center'>Welcome to SIM</h2>
  <h4 class='text-center'>Create Your OWN FUTURE!</h4>
  <form class='form-horizontal' action='LoginController.php' method='post'>
    <div class='form-group'>
      <label class='control-label col-sm-2' for='email'>Email:</label>
      <div class='col-sm-10'>
        <input type='email' class='form-control' placeholder='Enter email' name='Email'>
      </div>
    </div>
    <div class='form-group'>
      <label class='control-label col-sm-2' for='pwd'>Password:</label>
      <div class='col-sm-10'>          
        <input type='password' class='form-control' placeholder='Enter password' name='password'>
      </div>
    </div>
    <div class='form-group'>        
      <div class='col-sm-offset-2 col-sm-10'>
        <div class='checkbox'>
          <label><input type='checkbox' name='remember'> Remember me</label>
          <p></p>
          <p><a href='ForgetPasswordUI.php'>Forget Password?</a></p>
          <p></p>
          <p><a href='RegisterUI.php'>No account? Click here!</a></p>
        
      </div>
    </div>
    <div class='form-group'>        
      <div class='col-sm-offset-2 col-sm-10'>
        <button type='submit' class='btn btn-default' name='submit' value='Login'>Login</button>
      </div>
    </div>
  </form>
</div>";
	}
}

?>
<html>
<head>

	<title>Login</title>
  <meta charset='utf-8'>
  <meta name='viewport' content='width=device-width, initial-scale=1'>
  <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css'>
  <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js'></script>
  <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js'></script>    
</head>
    <body>
	<?php
        
        
        
		if (isset($_SESSION['loginStatus'])) {
			if($_SESSION['loginStatus'] == 'Invalid') {
				
				unset($_SESSION['loginStatus']);
			}
		}
		
		if (isset($_SESSION['registerStatus'])) {
			if($_SESSION['registerStatus'] == 'Yes') {
				echo 'Account Registered Successfully!<br/> Please Login <br/><br/>';
				//unset($_SESSION['registerStatus']);
			}
			
			else if($_SESSION['registerStatus'] == 'Registered') {
				echo 'Account Already Registered! <br/> Please Login <br/><br/>';
				//unset($_SESSION['registerStatus']);
			}
		}
		
		$user = new LoginUI();
		echo $user->displayLogin();
	?>
	</body>
</html>

