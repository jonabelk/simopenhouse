<?php session_start(); ?>

<?php
class ASL{
	
    private $image;
	private $title;
	private $links;
    
    
    public function setTitle($title) {
		$this->title = $title;
	}
	
	public function getTitle() {
		return $this->title;
	}
    
    public function setLink($links) {
		$this->links = $links;
	}
	
	public function getLink() {
		return $this->links;
	}
    
    public function setImage($image) {
		$this->image = $image;
	}
	
	public function getImage() {
		return $this->image;
	}
        
	
	function successRedirect() {
        ob_start();
        header('refresh:0;url=addStudentLife.php');
        ob_end_flush();
        die();
    }
	
	function emptyRedirect() {
        ob_start();
        
        header('refresh:4;url=addStudentLifeUI.php');
        
        //echo "<script>alert('One or more details not filled')</script>";
        ob_end_flush();
        die();
    }
    
    
    
	public function verifyForm() {
		if (
            
            ($_POST['title'] != "") &&
            ($_POST['link'] != "") &&
            ($_POST['image'] != "")
           )
        
        {
            
            
			//$this->setUserID($_POST['username']);
			$this->setTitle($_POST['title']);
			$this->setLink($_POST['link']);
            $this->setImage($_POST['image']);

            
			$_SESSION['title'] = $this->getTitle();
            $_SESSION['link'] = $this->getLink();
            $_SESSION['image'] = $this->getImage();
            
			
			$this->successRedirect();
		}
		else {
			
			$this->emptyRedirect();
		}
    }
	
}
?>

<html>
    <body>
	<?php 
	
	    $form = new ASL();
		
	    if (isset($_POST['submit'])){
            
            
		    $form->verifyForm();
            
           
            
            
		}
		else {
			$form->emptyRedirect();
		}
	?>
	</body>
</html>