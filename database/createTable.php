<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Create Table</title>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
</head>
<body>
<?php
    include 'inc_dbconnect.php';
    if ($conn===FALSE){
        die("connection failed");
    }
    
    // sql to create table
    $sql1 =
    
    "CREATE TABLE UserInformation(
    userID VARCHAR(30) NOT NULL PRIMARY KEY,
    FirstName VARCHAR(30) NOT NULL,
    LastName VARCHAR(30) NOT NULL,
    Phone INT(15) NOT NULL,
    Email VARCHAR(50) NOT NULL,
    password_md5 VARCHAR(32),
    Country VARCHAR(10) NOT NULL,
    userType VARCHAR(10) NOT NULL,
    Qualification VARCHAR(10) NOT NULL,
    AwardingInstitute VARCHAR(10) NOT NULL,
    YearOfGrad INT(4) NOT NULL,
    Interests VARCHAR(100) NOT NULL
    
    )";
    
    if (mysqli_query($conn, $sql1)) {
        echo "Table userInfo created successfully";
    } else {
        echo "Error creating table: " . mysqli_error($conn). "<br>";
    }
    
   
    
   
    /*$sql = "DROP TABLE PropertyInformation";
    if (mysqli_query($conn, $sql)) {
        echo "Table MyGuests dropped successfully";
    } else {
        echo "Error dropping table: " .                                  mysqli_error($conn);
    }*/
    
     


mysqli_close($conn);
?>
</body>
</html>
