<!DOCTYPE html>
<?php
    session_start();
    ob_start();    
    include("navbarUI.php");
    include("progController.php");
    
    $viewInterest = new Programme();
    
    if(isset($_POST['viewIntBut'])){
          $progID = key($_POST['viewIntBut']);
          $_POST['progID'] = $progID;

     }
     else{
         $progID = 0;
     }
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>View Interested Parties</title>
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    </head>
    <body>
           <?php   
           
                if(isset($_POST['viewIntBut'])){
                    $viewInterest->displayInterest($progID);
                }
                
            ?>
          </ul>
        </div>

    </body>
</html>
