<!DOCTYPE html>
<?php
    session_start();
    ob_start();
    include("navbarUI.php");
    include("eventsController.php");
    
    $_SESSION['displayMsg'] = "";
    
    $mgEvent = new Event();
    
     if(isset($_POST['mgBut'])){
          $eventID = key($_POST['mgBut']);
          $_POST['eventID'] = $eventID;

     }
     else{
         $eventID = 0;
     }
    
?>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>
        
        <?php
        
        if(isset($_POST['mgBut'])){
            $mgEvent->retrieveEvent($eventID);
        }
        elseif(isset($_POST['addBut'])){
            $mgEvent->addEventDetails();
        }
         
        
        if(isset($_POST['saveBut'])){
//                  $eventID = key($_POST['saveBut']);
//                  $_POST['eventID'] = $eventID;
            //echo $_POST['eventName'];

            $eventID = $_POST['eventID'];
            $mgEvent->saveChanges($eventID);
            echo $_SESSION['displayMsg'];
            $_SESSION['displayMsg'] = "";

        }
        
        if(isset($_POST['addEventBut'])){
            $mgEvent->addEvent();
            echo $_SESSION['displayMsg'];
            $_SESSION['displayMsg'] = "";
        }
        
        if(isset($_POST['removeBut'])){
            $eventID = $_POST['eventID'];
            $mgEvent -> removeEvent($eventID);
            echo $_SESSION['displayMsg'];
            $_SESSION['displayMsg'] = "";
        }
        
        
            
              
//              if($_SESSION['displayMsg'] != ""){
//                  echo "<script> alert('".$_SESSION['displayMsg']."')</script>";
//                  header("Refresh:0");
//                  
//              }
              ?>
        
    </body>
</html>

