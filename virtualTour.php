<?php
session_start();
include("navbarUI.php");
?>


<!DOCTYPE html>
<html lang="en">
<head>
  
  <title>Virtual Tour</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <style>
  body {
    font: 400 15px Lato, sans-serif;
    line-height: 1.8;
    color: #818181;
  }
  h2 {
    font-size: 24px;
    text-transform: uppercase;
    color: #303030;
    font-weight: 600;
    margin-bottom: 30px;
  }
  h4 {
    font-size: 19px;
    line-height: 1.375em;
    color: #303030;
    font-weight: 400;
    margin-bottom: 30px;
  }  


  .bg-grey {
    background-color: #f6f6f6;
  }

  </style>
</head>
<body>
<!-- Container (Contact Section) -->
<div id="contact" class="container-fluid bg-grey">
  <h2 class="text-center">Around SIM</h2>
    <h5 class="text-center">Haven't got the chance to look inside SIM? Here's your chance!</h5>
    <p class="text-center"><iframe  src="https://www.google.com/maps/embed?pb=!4v1615538618418!6m8!1m7!1sCAoSLEFGMVFpcFBWb2ZOZkFjU0JlQkVnRHhkNDNwOExQMWo4dkVxOURTR2xHa1du!2m2!1d1.3292746535588!2d103.77607974845!3f208.35670465846886!4f9.218372949813869!5f0.7820865974627469" width="800" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe></p>

</div>



</body>
</html>
