<?php session_start(); ?>

<?php

class Contact {

    private $email;
    private $name;
    private $msgContent;
    private $answer;
    private $userID;
    private $formType;
    private $status;

    public function setUserID($userID) {
        $this->userID = $userID;
    }

    public function getUserID() {
        return $this->userID;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getName() {
        return $this->name;
    }

    public function setMsgContent($msgContent) {
        $this->msgContent = $msgContent;
    }

    public function getMsgContent() {
        return $this->msgContent;
    }

    public function setAnswer($answer) {
        $this->answer = $answer;
    }

    public function getAnswer() {
        return $this->answer;
    }

    public function setFormType($formType) {
        $this->formType = $formType;
    }

    public function getFormType() {
        return $this->formType;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function getStatus() {
        return $this->status;
    }

    function successRedirect() {
        ob_start();
        header('refresh:0;url=contactUs.php');
        ob_end_flush();
        die();
    }

    function emptyRedirect() {
        ob_start();

        header('refresh:4;url=contactUsUI.php');

        //echo "<script>alert('One or more details not filled')</script>";
        ob_end_flush();
        die();
    }

    public function verifyForm() {
        if (
                ($_POST['name'] != "") &&
                ($_POST['msgStatus'] != "") &&
                ($_POST['comments'] != "")
        ) {


            
            $this->setName($_POST['name']);
//            $this->setEmail($_POST['email']);
            $this->setMsgContent($_POST['comments']);
//            $this->setAnswer("");
//            $this->setFormType("Enquiries");
            $this->setStatus('Awaiting');
            $this->setUserID($_SESSION['userID']);

            $_SESSION['name'] = $this->getName();
//            $_SESSION['email'] = $this->getEmail();
            $_SESSION['comments'] = $this->getMsgContent();
//            $_SESSION['answer'] = $this->getAnswer();
//            $_SESSION['formType'] = $this->getFormType();
            $_SESSION['status'] = $this->getStatus();
            


            $this->successRedirect();
        }
        else {

            $this->emptyRedirect();
        }
    }

}
?>

<html>
    <body>
<?php
    $form = new Contact();

    if (isset($_POST['submit'])) {
        $form->verifyForm();
    }
    else {
        $form->emptyRedirect();
    }
?>
    </body>
</html>