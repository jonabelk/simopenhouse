<!DOCTYPE html>
<?php
session_start();
ob_start();

$_SESSION['displayMsg'] = "";

include("navbarUI.php");
include("viewEnquiriesController.php");

$userEnq = new Enquiry();

if (isset($_POST['replyBut'])) {
    $threadID = key($_POST['replyBut']);
    $_POST['threadID'] = $threadID;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <!-- custom scrollbar plugin -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
    </head>
    <body>
        <div class="text-center">
            <h3>Inbox</h3>
            <p>Displaying all unanswered messages.</p>
        </div>
        <hr/>
        <?php
//    $userEnq->displaySubmitForm();
//    if(isset($_POST['submit'])){
//        $userEnq->submitEnquiry();
//        header("Refresh:0");
//    }
//    
//    echo "<hr/>";
//    

        $userEnq->displayEnquiries();

//        if (isset($_POST['replyBut'])) {
//            $threadID = $_POST['threadID'];
//            $fullname = $_POST['senderName'];
//            $ogSenderID = $_POST['ogSenderID'];
//            $userEnq->displayReplyForm($threadID, $fullname, $ogSenderID);
//            
//        }

        if (isset($_POST['replyFormBut'])) {
            $ogSenderID = $_POST['ogSenderID'];
            $replyMsgContent = $_POST['replyMsgContent'];
            $replySenderName = $_POST['replySenderName'];
            $rThreadID = $_POST['rThreadID'];
            $userEnq->submitReply($replySenderName, $replyMsgContent, $ogSenderID, $rThreadID);
            header("Refresh:0");
        }
        ?>



    </body>
</html>

<style>
    body{
        font-family: 'Raleway', sans-serif;

    }

</style>