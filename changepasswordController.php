<?php session_start(); ?>

<?php
class changePassword{
	
    private $userID;
	private $email;
	private $password;
    private $cpassword;
	
	public function setUserID ($userID) {
		$this->userID = $userID;
	}
	
	public function getUserID () {
		return $this->userID;
	}
	
	public function setPassword($password) {
		$this->password = $password;
	}
	
	public function getPassword() {
		return $this->password;
	}
    
    public function setCPassword($cpassword) {
		$this->cpassword = $cpassword;
	}
	
	public function getCPassword() {
		return $this->cpassword;
	}
	
    
	public function setEmail ($email) {
		$this->email = $email;
	}
	
	public function getEmail () {
		return $this->email;
	}
	
	function successRedirect() {
        ob_start();
       header('refresh:0;url=changePassword.php'); 
            
        ob_end_flush();
        die();
    }
	
	function emptyRedirect() {
        ob_start();
        
        header('refresh:0;url=changePasswordUI.php'); 
        
        echo "<script>alert('Please enter correct password')</script>";
        ob_end_flush();
        die();
    }
    
    
    
	public function change() {
		if (
            
            ($_POST['currentpw'] != "") &&
            ($_POST['password'] != "") &&
            ($_POST['confirmNewPW'] != "") &&
            ($_POST['confirmNewPW'] == $_POST['password'])

           )
        
        {
            
			$this->setEmail($_SESSION['Email']);
            $this->setPassword($_POST['password']);
            $this->setCPassword($_POST['currentpw']);
            
			$_SESSION['Email'] = $this->getEmail();
            $_SESSION['Password'] = $this->getPassword();
            $_SESSION['currentPassword'] = $this->getcPassword();
            
			
			$this->successRedirect();
		}
		else {
			
			$this->emptyRedirect();
		}
    }
	
}
?>

<html>
    <body>
	<?php 
	
	    $user = new changePassword();
		
	    if (isset($_POST['submit'])){
            
            
            
		    $user->change();
            
           
            
            
		}
		else {
			$user->emptyRedirect();
		}
	?>
	</body>
</html>