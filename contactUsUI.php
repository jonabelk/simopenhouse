<?php
session_start();
include("navbarUI.php");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Theme Made By www.w3schools.com -->
        <title>Contact Us</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <style>
            body {
                font: 400 15px Lato, sans-serif;
                line-height: 1.8;
                color: #818181;
            }
            h2 {
                font-size: 24px;
                text-transform: uppercase;
                color: #303030;
                font-weight: 600;
                margin-bottom: 30px;
            }
            h4 {
                font-size: 19px;
                line-height: 1.375em;
                color: #303030;
                font-weight: 400;
                margin-bottom: 30px;
            }  


            .bg-grey {
                background-color: #f6f6f6;
            }

            .map-responsive{
                overflow:hidden;
                padding-bottom:50%;
                position:relative;
                height:0;
            }

            .map-responsive iframe{
                left:0;
                top:0;
                height:100%;
                width:100%;
                position:absolute;
            }

        </style>
    </head>
    <body>
        <!-- Container (Contact Section) -->
        <div id="contact" class="container-fluid bg-grey">
            <h2 class="text-center">CONTACT US</h2>
            <div class="border-left">
                <div class="row">
                    <div class="col-sm-5">

                        <p><b>SIM HEADQUARTERS</b></p>
                        <p><span class="glyphicon glyphicon-map-marker"></span> 461 Clementi Road</p>
                        <p><span class="glyphicon glyphicon-phone"></span> +65 6248 9746</p>
                        <p><span class="glyphicon glyphicon-envelope"></span> study@sim.edu.sg</p>
                        <p>

                        <div class="map-responsive">
                            <iframe width="400" height="200" id="gmap_canvas" src="https://maps.google.com/maps?q=singapore%20Institute%20of%20management&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><br>
                        </div>
                        <br/>
                        </p>
                    </div>

                    <div class="row" style="padding-left:15px; padding-right:15px;">
                        <div class="col-6 border-right"></div>

                        <form role='form' method='POST' action ='ContactUsController.php'>
                            <div class="col-sm-6 form-group" style="margin-bottom:0px;">
                                <p>Ask away and we'll try get back to you as soon as possible!</p>
                            </div>

                            <div class="col-sm-6 form-group">
                                <input class="form-control" id="name" name="name" placeholder="Name" type="text" 
                                       value="<?php
                                       if (isset($_SESSION['FirstName'])) {
                                           echo $_SESSION['FirstName'] . " " . $_SESSION['LastName'];
                                       }
                                       ?>" required>
                            </div>

                            <!--<div class="col-sm-6 form-group">
                              <input class="form-control" id="email" name="email" placeholder="Email" type="email" required
                            value="<?php
                            if (isset($_SESSION['Email'])) {
                                echo $_SESSION['Email'];
                            }
                            ?>">
                            </div>-->

                            <div class="col-sm-6 form-group">
                                <textarea class="form-control" id="comments" name="comments" placeholder="Type out your message" rows="5" required></textarea><br>
                            </div>

                            <input id="msgStatus" name="msgStatus" placeholder="Message Status" type="text" value="Awaiting" hidden required>

                            <div class="col-sm-6 form-group text-center">
                                <button class="btn btn-default" type="submit" name ="submit">Send</button>
                            </div>

                        </form>
                    </div>
                </div>



            </div>

        </div>



    </body>
</html>
