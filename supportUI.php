<?php
session_start();
include("navbarUI.php");


?>
<!DOCTYPE html>
<html lang="en">
<head>
  
  <title>Contact Us</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <style>
  body {
    font: 400 15px Lato, sans-serif;
    line-height: 1.8;
    color: #818181;
  }
  h2 {
    font-size: 24px;
    text-transform: uppercase;
    color: #303030;
    font-weight: 600;
    margin-bottom: 30px;
  }
  h4 {
    font-size: 19px;
    line-height: 1.375em;
    color: #303030;
    font-weight: 400;
    margin-bottom: 30px;
  }  


  .bg-grey {
    background-color: #f6f6f6;
  }

  </style>
</head>
<body>
<!-- Container (Contact Section) -->
<div id="contact" class="container-fluid bg-grey">
  <h2 class="text-center">SUPPORT</h2>
  <div class="row">
    <div class="col-sm-5">
      <p>Facing a problem on the website? Let us know we'll try to get it fixed.</p>
        <p><b>SIM HEADQUARTERS</b></p>
      <p><span class="glyphicon glyphicon-map-marker"></span> 461 Clementi Road</p>
      <p><span class="glyphicon glyphicon-phone"></span> +65 6248 9746</p>
      <p><span class="glyphicon glyphicon-envelope"></span> study@sim.edu.sg</p>
        <p>
        
        
                <iframe width="400" height="200" id="gmap_canvas" src="https://maps.google.com/maps?q=singapore%20Institute%20of%20management&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><br>
        
        </p>
    </div>
    
      <form role='form' method='POST' action ='SupportController.php'>
      
      <div class="row">
          
          
          
        <div class="col-sm-6 form-group">
          <input class="form-control" id="name" name="name" placeholder="Name" type="text" required 
            value="<?PHP if(isset($_SESSION['FirstName'])){echo $_SESSION['FirstName'] . " " . $_SESSION['LastName'];}?>">
        </div>
          
        <div class="col-sm-6 form-group">
          <input class="form-control" id="email" name="email" placeholder="Email" type="email" required
        value="<?PHP if(isset($_SESSION['Email'])){echo $_SESSION['Email'];}?>">
        </div>
          
          <div class="col-sm-6 form-group">
          <textarea class="form-control" id="comments" name="comments" placeholder="Facing a Problem?" rows="5"></textarea><br>
        </div>
          
      </div>
      
      <div class="row">
        <div class="col-sm-12 form-group">
          <button class="btn btn-default pull-right" type="submit" name ="submit">Send</button>
      </div>
    </div>
      
      </form>
      
  </div>
      
</div>



</body>
</html>
