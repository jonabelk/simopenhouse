<?php session_start(); ?>

<?php
class ASL {
	private $conn = NULL;
	
    private $image;
	private $title;
	private $links;
	
	function __construct() {
		include ("SIMOpenHouseDB.php");
		$this->conn = $conn;
	}
	
    
	public function setTitle($title) {
		$this->title = $title;
	}
	
	public function getTitle() {
		return $this->title;
	}
    
    public function setLink($links) {
		$this->links = $links;
	}
	
	public function getLink() {
		return $this->links;
	}
    
    public function setImage($image) {
		$this->image = $image;
	}
	
	public function getImage() {
		return $this->image;
	}
	
	public function verifyForm() {
        
		$this->setTitle($_SESSION['title']);
        $this->setLink($_SESSION['link']);
        $this->setImage($_SESSION['image']);
		
		unset($_SESSION['title']);
		unset($_SESSION['link']);
		unset($_SESSION['image']);
		
		$sql = "SELECT * FROM studentlife";
		
		
		$result = @sqlsrv_query($this->conn, $sql);
		if ($result !== FALSE)
		{
			while (($row = sqlsrv_fetch_array($result)) != FALSE)
			{
				if (
                $this->getTitle() != $row['Title'] ||
                $this->getLink() != $row['Link'] ||
                $this->getImage() != $row['Image']
                
                ){
					continue;
				}
				
				else {
				$this->successRedirect();
				}
			}
			
			$sql1 = "INSERT INTO studentlife" .
					" (Title, Link, Image) " .
					 " VALUES " .
					 " ('".$this->getTitle()."', '".$this->getLink()."', '".$this->getImage()."')";

			$result1 = sqlsrv_query($this->conn, $sql1);

			if ($result1 === FALSE)
			  echo "Unable to execute the query"
					. $this->conn->connect_errno 
					. $this->conn->connect_error;
            
                    
            
			else {
				
				$this->successRedirect();
			}
		}
		
		else {
			echo "Unable to execute the query."
				. $this->conn->connect_errno 
				. $this->conn->connect_error;
		}
		
	}
	
	function successRedirect() {
        ob_start();
        header("refresh:0;url=studentLifeUI.php" );
         echo "<script>alert('Added.')</script>";
        ob_end_flush();
        die();
    }
	 
	function failedRedirect() {
        ob_start();
        header( "refresh:0;url=AddStudentLifeUI.php" );
        echo "<script>alert('Add Fail')</script>";
        ob_end_flush();
        die();
    }
	
	function __wakeup() {
		include("SIMOpenHouseDb.php");
		$this->conn = $conn;
	}

	function __destruct() {
		if (!$this->conn->connect_error)
			@$this->conn->close();
	}
}
?>

<html>
    <body>
        
        <?php
        
		$form = new ASL();
     
        $form->verifyForm();
			
	?>
	</body>
</html>