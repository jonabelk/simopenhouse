<!DOCTYPE html>
<?php
session_start();
ob_start();

$_SESSION['displayMsg'] = "";

include("navbarUI.php");
include("viewEnquiriesController.php");

$userEnq = new Enquiry();

if (isset($_POST['replyBut'])) {
    $threadID = key($_POST['replyBut']);
    $_POST['threadID'] = $threadID;
}

if (isset($_POST['resolvedBut'])) {
    $threadID = key($_POST['resolvedBut']);
    $_POST['threadID'] = $threadID;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <!-- custom scrollbar plugin -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
    </head>
    <body>
        <div class="text-center">
            <h3>Your Inbox</h3>
            <p>Displaying all your conversations.</p>
            <p style="font-size:10px;">(Displays only your latest 2 messages)</p>
        </div>

        <div class='pull-right' style='margin-right:30px; margin-top: -30px; margin-bottom:20px;'>
            <form method="POST" action="viewMessagesUI.php">
                <?php if ($_SESSION['changeBut'] = 1) { ?>
                    <button name='viewResolvedBut' class='btn btn-primary' >View Resolved Messages</button>
                    <?php
                }
                if ($_SESSION['changeBut'] = 0) {
                    ?>
                    <button name='viewUnresolvedBut' class='btn btn-primary' >View Unresolved Messages</button>
                <?php } ?>
                <input id="submitbutton" type="button" value="View Resolved Messages" />

                <br/>
            </form>
        </div>

        <?php
        $userID = $_SESSION['userID'];
        $fullName = $_SESSION['FirstName'] . " " . $_SESSION['LastName'];

        if (isset($_POST['viewResolvedBut'])) {
            $userEnq->displayUsersMessages($userID, $fullName, "Yes");
            $_SESSION['changeBut'] = 1;
            echo "<script>document.getElementById('submitbutton').value='View Unresolved Messages'</script>";
        }
        elseif (isset($_POST['viewUnresolvedBut'])) {
            $userEnq->displayUsersMessages($userID, $fullName, "No");
            $_SESSION['changeBut'] = 0;
            echo "<script>document.getElementById('submitbutton').value='View Resolved Messages'</script>";
        }
        else {
            $userEnq->displayUsersMessages($userID, $fullName, "No");
            $_SESSION['changeBut'] = 0;
            echo "<script>document.getElementById('submitbutton').value='View Resolved Messages'</script>";
        }
        ?>

        <?php
        if (isset($_POST['resolvedBut'])) {
            $threadID = $_POST['threadID'];
            $userEnq->queryResolved($threadID);
            header("Refresh:0");
        }
        ?>


    </body>
</html>

<!--<script type="text/javascript">
    function changeText(submitId) {

        var submit = document.getElementById(submitId);
        submit.value = 'View Resolved Messages';

        return true;
//        var isResolved = 'No';
//
//
//        var isResolved;
//
//        isResolved = 'No';
//
//        if (isResolved = 'Yes') {
//            var submit = document.getElementById(submitId);
//            submit.value = 'View Resolved Messages';
//
//            return false;
//            var isResolved = 'No';
//
//        } else if (isResolved = 'No') {
//            var submit = document.getElementById(submitId);
//            submit.value = 'View Unresolved Messages';
//
//            return false;
//            var isResolved = 'Yes';
//
//        }

    }
    ;
</script>-->



<style>
    body{
        font-family: 'Raleway', sans-serif;
    }
</style>


