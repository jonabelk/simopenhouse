<?php
session_start();
include("navbarUI.php");
?>

<!DOCTYPE html>
<html lang="en">
<head>
  
  <title>Contact Us</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
    
<link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css'>
    <script src='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js'></script>
    <link href='https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css'>
    <style>

.card {
    border: none
}

.image {
    position: relative
}

.image span {
    background-color: blue;
    color: #fff;
    padding: 6px;
    height: 30px;
    width: 30px;
    border-radius: 50%;
    font-size: 13px;
    position: absolute;
    display: flex;
    justify-content: center;
    align-items: center;
    top: -0px;
    right: 0px
}

.user-details h4 {
    color: blue
}

.ratings {
    font-size: 30px;
    font-weight: 600;
    display: flex;
    justify-content: left;
    align-items: center;
    color: #f9b43a
}

.user-details span {
    text-align: left
}

.inputs label {
    display: flex;
    margin-left: 3px;
    font-weight: 500;
    font-size: 13px;
    margin-bottom: 4px
}

.inputs input {
    font-size: 14px;
    height: 40px;
    border: 2px solid #ced4da
}

.inputs input:focus {
    box-shadow: none;
    border: 2px solid blue
}

.about-inputs label {
    display: flex;
    margin-left: 3px;
    font-weight: 500;
    font-size: 13px;
    margin-bottom: 4px
}

.about-inputs textarea {
    font-size: 14px;
    height: 100px;
    border: 2px solid #ced4da;
    resize: none
}

.about-inputs textarea:focus {
    box-shadow: none
}

.btn {
    font-weight: 600
}

.btn:focus {
    box-shadow: none
}
    
    
    </style>
</head>
    
    <body>
    
    <div class="container bootstrap snippets bootdey">
    <h1 class="text-primary"><span class="glyphicon glyphicon-user"></span>Update Password</h1>
      <hr>
	<div class="row">
      
      <!-- edit form column -->
      <div class="col-md-9 personal-info">
        <form class="form-horizontal" role="form" method="post" action="changepasswordController.php">
          <div class="form-group">
            <label class="col-lg-3 control-label">Current Password:</label>
            <div class="col-lg-8">
              <input class="form-control" name="currentpw" type="password" >
            </div>
          </div>
         
          <div class="form-group">
            <label class="col-lg-3 control-label">New Password</label>
            <div class="col-lg-8">
              <input class="form-control" type="password" name="password">
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-3 control-label">Confirm Password:</label>
            <div class="col-lg-8">
              <input class="form-control" type="password" name="confirmNewPW">
            </div>
          </div>

            <div class="form-group">
            
            <div class="col-lg-8">
              <button type="submit" name="submit">Save</button>
            </div>
          </div>
            
            
        </form>
      </div>
  </div>
</div>
<hr>
    
    </body>
</html>