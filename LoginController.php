<?php session_start(); ?>

<?php
class LoginController {
	
	private $username;
	private $password;
	
	public function setUsername($username) {
		$this->username = $username;
	}
    
    public function getUsername() {
		return $this->username;
	}
	
	public function setPassword($password) {
		$this->password = $password;
	}
    
    public function getPassword() {
		return $this->password;
	}
	
	function successRedirect() {
        ob_start();
        header('Location: Login.php');
        ob_end_flush();
        die();
    }
	
	function emptyRedirect() {
        ob_start();
        header('Location: loginUI.php');
        ob_end_flush();
        die();
    }
	
	public function verifyLogin() {
		if (
            ($_POST['Email'] != "") && ($_POST['password'] != "")
        ){
			$this->setUsername($_POST['Email']);
			$this->setPassword($_POST['password']);
			$_SESSION['Email'] = $this->getUsername();
			$_SESSION['password'] = $this->getPassword();
			$this->successRedirect();
		}
		
		else {
			$this->emptyRedirect();
		}
    }
	
}
?>

<html>
	<body>
	<?php
        
	    $user = new LoginController();
		
	    if (isset($_POST['submit'])){
		    $user->verifyLogin();
		}
		else {
			$user->emptyRedirect();
		}
	?>
	</body>
</html>