<?php session_start(); ?>

<?php
class manageAccount{
	
    private $userID;
	private $email;
	private $name;
	private $phone;
	
    
    
	public function setUserID ($userID) {
		$this->userID = $userID;
	}
	
	public function getUserID () {
		return $this->userID;
	}
	
	public function setPhone ($phone) {
		$this->phone = $phone;
	}
	
	public function getPhone () {
		return $this->phone;
	}
	
	public function setEmail ($email) {
		$this->email = $email;
	}
	
	public function getEmail () {
		return $this->email;
	}
    
    public function setName ($name) {
		$this->name = $name;
	}
	
	public function getName () {
		return $this->name;
	}
	
	
	
	function successRedirect() {
        ob_start();
        header('refresh:0;url=manageAccount.php'); 
            
        ob_end_flush();
        die();
    }
	
	function emptyRedirect() {
        ob_start();
        
        header('refresh:4;url=manageAccountUI.php'); 
        
        //echo "<script>alert('One or more details not filled')</script>";
        ob_end_flush();
        die();
    }
    
    
    
	public function manage() {
		if (
            
            ($_POST['Name'] != "") &&
            ($_POST['Phone'] != "") &&
            ($_POST['Email'] != "")

           )
        
        {
            
            
			//$this->setUserID($_POST['username']);
			$this->setName($_POST['Name']);
			$this->setPhone($_POST['Phone']);
			$this->setEmail($_POST['Email']);
            
			$_SESSION['Name'] = $this->getName();
            $_SESSION['Phone'] = $this->getPhone();
            $_SESSION['Email'] = $this->getEmail();
            
			
			$this->successRedirect();
		}
		else {
			
			$this->emptyRedirect();
		}
    }
	
}
?>

<html>
    <body>
	<?php 
	
	    $user = new manageAccount();
		
	    if (isset($_POST['submit'])){
            
            
            
		    $user->manage();
            
           
            
            
		}
		else {
			$user->emptyRedirect();
		}
	?>
	</body>
</html>