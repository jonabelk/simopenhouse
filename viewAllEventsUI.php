<!DOCTYPE html>
<?php
    session_start();
    ob_start();
    
    $_SESSION['displayMsg'] = "";
    
    include("navbarUI.php");
    include("eventsController.php");
    
    $event = new Event();
    
    if(isset($_POST['signedUp'])){
          $eventID = key($_POST['signedUp']);
          $_POST['eventID'] = $eventID;

     }
     elseif(isset($_POST['cancelSignUp'])){
         $eventID = key($_POST['cancelSignUp']);
         $_POST['eventID'] = $eventID;
     }
     else{
         $eventID = 0;
     }
?>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/main.css">
        <title>View All Events</title>
    </head>
    <body>
        <div class="text-center">
            <h3>All Events</h3>
            <p>Listing all events.</p>
        </div>
       
        <section class='viewAllEvents'>
            <?php $event->displayAllEvents(); ?>
            
            <?php 
                if(isset($_POST['signedUp'])){
                    $userID = $_SESSION['userID'];
                    $eventID = $_POST['eventID'];
                    $event->signUpEvent($eventID, $userID);
                    header("Refresh:0");
                }
                
                if(isset($_POST['cancelSignUp'])){
                    $userID = $_SESSION['userID'];
                    $eventID = $_POST['eventID'];
                    $event->cancelSignUp($eventID, $userID);
                    header("Refresh:0");
           
                }
            
            ?>
                  
        </section>
           
    </body>
</html>
