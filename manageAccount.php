<?php session_start(); ?>

<?php

class manageAccount {
	private $conn = NULL;
	
	private $userID;
	private $email;
	private $name;
	private $phone;
	
	
	function __construct() {
		include ("simopenhousedb.php");
		
		$this->conn = $conn;
		
		$this->name = $_SESSION['Name'];
		
		$this->email = $_SESSION['Email'];
		
		$this->phone = $_SESSION['Phone'];
	}
	
	public function setUserID ($userID) {
		$this->userID = $userID;
	}
	
	public function getUserID () {
		return $this->userID;
	}
	
	public function setPhone ($phone) {
		$this->phone = $phone;
	}
	
	public function getPhone () {
		return $this->phone;
	}
	
	public function setEmail ($email) {
		$this->email = $email;
	}
	
	public function getEmail () {
		return $this->email;
	}
    
    public function setName ($name) {
		$this->name = $name;
	}
	
	public function getName () {
		return $this->name;
	}
	
	public function manage() {
		
		$sql = "UPDATE userinformation SET 
				Email='".$this->getEmail()."' 
				,Phone='".$this->getPhone()."' WHERE Email='".$this->getEmail()."'";
			
		$result = sqlsrv_query($this->conn, $sql);
		
		if ($result === FALSE)
			  echo "Unable to execute the query"
					. $this->conn->connect_errno 
					. $this->conn->connect_error;
		else {
			$_SESSION['statusUpdated'] = 'Added';
			$this->successRedirect();
		}
		
		
	}
	
	function successRedirect() {
        ob_start();
        header('refresh:0;url=viewProfile.php');
        echo "<script>alert('Profile Updated.')</script>";
        ob_end_flush();
        die();
    }
	
	function failRedirect() {
        ob_start();
        header('refresh:4;url=manageAccountUI.php'); 
        echo "<script>alert('Update failed.')</script>";
        ob_end_flush();
        die();
    }
	
	function __wakeup() {
		include("simopenhousedb.php");
		$this->conn = $conn;
	}

	function __destruct() {
		if (!$this->conn->connect_error)
			@$this->conn->close();
	}
}
?>
<html>
    <body>
	<?php
	    $updateAccount = new manageAccount();
			$updateAccount->manage();
		
	?>
	</body>
</html>
