<?php
include ('navbarUI.php');

?>
<!DOCTYPE html>
	<html>
	<head>
	<meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/main.css">
        <title>Student Life</title>
		
	</head>

	<body>
		<!-------------------Main Content------------------------------>
		<div class="container main">
			<h3 class="text-center">Showing Images from database</h3>
            
            <?php
            include ('SIMOpenHouseDB.php');
            
            $image_query = sqlsrv_query($result,"select Image, Link, Title from studentlife");
	while($rows = sqlsrv_fetch_array($image_query))
	{
		$img_name = $rows['Title'];
		$img_src = $rows['Image'];
        $link = $rows['Link'];
	?>
            
            
			<div class="img-box">
                
                <img src="<?php echo $img_src; ?>" alt="" title="<?php echo $img_name; ?>" class="img-responsive" />
                
	<p><strong><?php echo $img_name; ?></strong></p>
                
			</div>
            
            <?php
    }
            ?>
            
		</div>
	</body>
	</html>