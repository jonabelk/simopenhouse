<!DOCTYPE html>
<?php
session_start();
ob_start();

$_SESSION['displayMsg'] = "";

include("navbarUI.php");
include("viewEnquiriesController.php");

$userEnq = new Enquiry();



if (isset($_POST['viewConvoBut'])) {
    $threadID = key($_POST['viewConvoBut']);
    $_POST['threadID'] = $threadID;
}

if (isset($_POST['replyBut'])) {
    $threadID = key($_POST['replyBut']);
    $_POST['threadID'] = $threadID;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <!-- custom scrollbar plugin -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
    </head>
    <body>
        <?php
        if (isset($_POST['viewConvoBut'])) {
            $threadID = $_POST['threadID'];
            $userID = $_SESSION['userID'];
            $fullName = $_SESSION['FirstName'] . " " . $_SESSION['LastName'];
            $_SESSION['reloadConvo'] = 'No';
            $userEnq->displayConvo($threadID, $fullName);
            
        }
        
        if (isset($_POST['replyBut'])) {
            $threadID = $_POST['threadID'];
            $userID = $_SESSION['userID'];
            $fullName = $_SESSION['FirstName'] . " " . $_SESSION['LastName'];
            $_SESSION['reloadConvo'] = 'No';
            $userEnq->displayConvo($threadID, $fullName);
            
        }
        ?>
        <?php
        if (isset($_POST['replyFormBut'])) {
            $recipientID = $_POST['recipientID'];
            $replyMsgContent = $_POST['replyMsgContent'];
            $replySenderName = $_POST['replySenderName'];
            $rThreadID = $_POST['rThreadID'];
            $userEnq->submitReply($replySenderName, $replyMsgContent, $recipientID, $rThreadID);
            header("Refresh:0");
        }
        ?>

        <?php
        if (isset($_SESSION['reloadConvo']) && ($_SESSION['reloadConvo'] == 'Yes')) {
            $threadID = $_SESSION['threadID'];
            $fullName = $_SESSION['fullName'];
            $userEnq->displayConvo($threadID, $fullName); 
            
        }
        if ((isset($_POST['viewConvoBut'])) && isset($_SESSION['reloadConvo']) && ($_SESSION['reloadConvo'] == 'No')) {
            unset($_SESSION['reloadConvo']);
            unset($_SESSION['threadID']);
            unset($_SESSION['fullName']);
        }
         if ((isset($_POST['replyBut'])) && isset($_SESSION['reloadConvo']) && ($_SESSION['reloadConvo'] == 'No')) {
            unset($_SESSION['reloadConvo']);
            unset($_SESSION['threadID']);
            unset($_SESSION['fullName']);
        }
        ?>

    </body>
</html>

<style>
    body{
        font-family: 'Raleway', sans-serif;

    }

</style>