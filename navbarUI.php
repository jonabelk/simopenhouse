<!DOCTYPE html>

<?php
    //print_r($_SESSION);
    //session_unset();
    
    if(isset($_POST['logout'])){
        session_destroy();
        header('Refresh: 0; URL = index.php');
    }
    
    
    
?>

<html>
    <head>
        <title>SIM Open House 2021</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <!--<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/main.css">
        
    </head>
    <body>
       <!-- BEGIN DISPLAYING GENERAL NAVIGATION BAR -->
        <nav class="navbar navbar-inverse" style="margin-bottom: 0px; border-radius:0px;">
          <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavBar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                </button>
                <span><a href="index.php"><img src="https://www.simge.edu.sg/wp-content/uploads/2019/11/SIMGE-logo-white.png" alt="Image" style="padding-top: 10px; padding-bottom: 20px; padding-left: 5px" width="150"></a></span>
            </div>
            <div class="collapse navbar-collapse" id="myNavBar">
        <!-- END OF DISPLAYING GENERAL NAVIGATION BAR -->
        
        <!-- DISPLAYING FOR NON-USERS -->
        <?php
        if((!isset($_SESSION['Email']))){
        ?>
        <ul class="nav navbar-nav">
            <li><a href='viewAllEventsUI.php'>Our Events</a></li>
            <li><a href="viewAllProgUI.php">Our Programmes</a></li>
            <li><a href="studentLifeUI.php">Student Life</a></li>
            <li><a href="virtualTour.php">Virtual Tour</a></li>
            <li><a href="contactUsUI.php">Contact Us</a></li>
            <li><a href="supportUI.php">Support</a></li>
        </ul>
        <form action="index.php" method="POST">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <div class="navbar-form navbar-right">
                        <div class="input-group" width="50%">
                          <input type="text" class="form-control" placeholder="Search">
                          <div class="input-group-btn">
                            <button class="btn btn-default" type="submit">
                              <i class="glyphicon glyphicon-search"></i>
                            </button>
                          </div>
                        </div>
                    </div>
                </li>
                <li><a href="RegisterUI.php"><span class="glyphicon glyphicon-user"></span> Register</a></li>
                <!-- ADD THIS AFTER LOGIN PAGE RECEIVED FROM JOEY -->
                <li><a href="LoginUI.php" name="login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li> 
                
            </ul>
        </form>    
        <?php 
        }
        ?>
        <!-- END OF DISPLAYING FOR NON-USERS -->    
        
        <!-- DISPLAYING FOR STUDENTS/PARENTS/OTHERS -->
        <?php
            if((isset($_SESSION['userType'])) AND ($_SESSION['userType']) == 'Student' || ($_SESSION['userType']) == 'Parent' || ($_SESSION['userType']) == 'Others'){
        ?>    
        <ul class="nav navbar-nav">
            <li><a href='viewAllEventsUI.php'>Our Events</a></li>
            <li><a href="viewAllProgUI.php">Our Programmes</a></li>
            <li><a href="studentLifeUI.php">Student Life</a></li>
            <li><a href="virtualTour.php">Virtual Tour</a></li>
            <li><a href="contactUsUI.php">Contact Us</a></li>
            <li><a href="supportUI.php">Support</a></li>
        </ul>
        <form action="index.php" method="POST">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <div class="navbar-form navbar-right">
                        <div class="input-group" width="50%">
                          <input type="text" class="form-control" placeholder="Search">
                          <div class="input-group-btn">
                            <button class="btn btn-default" type="submit">
                              <i class="glyphicon glyphicon-search"></i>
                            </button>
                          </div>
                        </div>
                    </div>
                </li>
                
                <li><a href="viewMessagesUI.php"><span class="glyphicon glyphicon-envelope"></span> Inbox</a></li>
                                        
                <!-- DISPLAYING DROP DOWN BAR -->
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Welcome <?php echo $_SESSION['FirstName'];?> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="viewProfile.php">View Profile</a></li>
                        <li><a href="manageAccountUI.php">Manage Account</a></li>
                        <li><button name='logout' type="submit" class="btn btn-link navbar-btn"><span class="glyphicon glyphicon-log-out"></span> Logout</button></li>
                    </ul>
                </li>
                <!-- END OF DROP DOWN BAR -->
                
            </ul>
        </form>    
        <?php
            }
        ?>
        <!-- END OF DISPLAYING RIGHT NAV BAR FOR STUDENTS/PARENTS/OTHERS -->
        
        <!-- DISPLAYING FOR SIM STAFF AND SIM PARTNERS -->
        <?php
            if((isset($_SESSION['userType'])) AND ((($_SESSION['userType']) == 'SIMPartner') OR ($_SESSION['userType']) == 'SIMStaff')){
        ?>    
        <ul class="nav navbar-nav">
            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Events<span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="viewAllEventsUI.php">View Events</a></li>
                    <li><a href="viewPastEventsUI.php">View Past Events</a></li>
                    <li><a href="reviewEventsUI.php">Review Events</a></li>
                </ul>
            </li>
            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Programmes<span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="viewAllProgUI.php">View Programmes</a></li>
                    <li><a href="reviewProgUI.php">Review Programes</a></li>
                </ul>
            </li>
            <?php if((isset($_SESSION['userType'])) AND ((($_SESSION['userType']) == 'SIMStaff'))){?>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Forms<span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="viewUnansweredEnqUI.php">View All Unanswered Enquiries</a></li>
                </ul>
            </li>
            <?php } ?>
            
            
        </ul>
        <form action="index.php" method="POST">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <div class="navbar-form navbar-right">
                        <div class="input-group" width="50%">
                          <input type="text" class="form-control" placeholder="Search">
                          <div class="input-group-btn">
                            <button class="btn btn-default" type="submit">
                              <i class="glyphicon glyphicon-search"></i>
                            </button>
                          </div>
                        </div>
                    </div>
                </li>
                <?php if((isset($_SESSION['userType'])) AND ((($_SESSION['userType']) == 'SIMStaff'))){ ?>
                <li><a href="viewMessagesUI.php"><span class="glyphicon glyphicon-envelope"></span> Inbox</a></li>
                <?php } ?>
                
                <!-- DISPLAYING DROP DOWN BAR -->
                <li class="dropdown d-inline-block"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Welcome <?php echo $_SESSION['FirstName'];?> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="viewStaffProfile.php">View Profile</a></li>
                        <li><a href="#">Manage Account</a></li>
                        <li><button name="logout" type="submit" class="btn btn-link navbar-btn"><span class="glyphicon glyphicon-log-out"></span> Logout</button></li>
                    </ul>
                </li>
                <!-- END OF DROP DOWN BAR -->
                
                
               
            </ul>
        </form>    
        <?php
            }
        ?>        
        <!-- END OF DISPLAYING FOR SIM STAFF AND SIM PARTNERS -->
          </div>
          </div>
        </nav>

    
    </body>
</html>

<script>    
var coll = document.getElementsByClassName("dropdown-toggle");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.display === "block") {
      content.style.display = "none";
      this.classList.toggle("inactive");
    } else {
      content.style.display = "block";
    }
  });
} 

var coll = document.getElementsByClassName("dropdown");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    //this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.display === "block") {
      content.style.display = "none";
      this.classList.toggle("inactive");
    } else {
      content.style.display = "block";
    }
  });
}
</script>

<!--<style>
    body{
        font-family: 'Raleway', sans-serif;

    }
</style>-->