<!DOCTYPE html>
<?php
    session_start();
    ob_start();

    $_SESSION['displayMsg'] = "";

    include("navbarUI.php");
    include("progController.php");

    $prog = new Programme();

    if (isset($_POST['indicateIntBut'])) {
        $progID = key($_POST['indicateIntBut']);
        $_POST['progID'] = $progID;
    }
    elseif (isset($_POST['cancelIndicateBut'])){
         $progID = key($_POST['cancelIndicateBut']);
         $_POST['progID'] = $progID;
     }
     else{
         $progID = 0;
     }
?>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/main.css">
        <title>View All Events</title>
    </head>
    <body>
        <div class="text-center">
            <h3>All Programmes</h3>
            <p>Listing all programmes.</p>
        </div>
        <section class='viewAllProgrammes'>
            <?php $prog->displayAllProg(); ?>

            <?php
                if (isset($_POST['indicateIntBut'])) {
                    $userID = $_SESSION['userID'];
                    $progID = $_POST['progID'];
                    $prog->indicateInt($progID, $userID);
                    header("Refresh:0");
                }
                if (isset($_POST['cancelIndicateBut'])){
                    $userID = $_SESSION['userID'];
                    $progID = $_POST['progID'];
                    $prog->cancelIndicate($progID, $userID);
                    header("Refresh:0");
           
                }
            ?>

        </section>

    </body>
</html>
