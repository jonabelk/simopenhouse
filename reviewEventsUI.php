<!DOCTYPE html>
<?php
    session_start();
    ob_start();
    
    $_SESSION['displayMsg'] = "";
    
    include("navbarUI.php");
    include("eventsController.php");
    
    $rEvent = new Event();
    
    if(isset($_POST['approveBut'])){
          $eventID = key($_POST['approveBut']);
          $_POST['eventID'] = $eventID;

     }
     elseif(isset($_POST['rejectBut'])){
         $eventID = key($_POST['rejectBut']);
         $_POST['eventID'] = $eventID;
     }
     else{
         $eventID = 0;
     }
?>

<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="css/main.css">
        <title>Review Events</title>
    </head>
    <body>
        <!--<div class="text-center">
            <h3>Review Events</h3>
            <p>Listing all newly added events for your approval.</p>
        </div>-->
       
        <section class='reviewEvents'>
            <?php $rEvent->displayReview(); ?>
            
            <?php 
                if(isset($_POST['approveBut'])){
                    $userID = $_SESSION['userID'];
                    $eventID = $_POST['eventID'];
                    $status = 'Approved';
                    $rEvent->updateReviewStatus($eventID, $userID, $status);
                    header("Refresh:0");
                }
                if(isset($_POST['rejectBut'])){
                    $userID = $_SESSION['userID'];
                    $eventID = $_POST['eventID'];
                    $status = 'Rejected';
                    $rEvent->updateReviewStatus($eventID, $userID, $status);
                    header("Refresh:0");
                }
            ?>
        </section>
           
    </body>
</html>
