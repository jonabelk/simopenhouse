<?php session_start(); ?>

<?php

class ContactUs {

    private $conn = NULL;
    private $email;
    private $name;
    private $msgContent;
    private $answer;
    private $userID;
    private $formType;
    private $status;

    function __construct() {
        include ("SIMOpenHouseDB.php");
        $this->conn = $conn;
    }

    public function setUserID($userID) {
        $this->userID = $userID;
    }

    public function getUserID() {
        return $this->userID;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getName() {
        return $this->name;
    }

    public function setMsgContent($msgContent) {
        $this->msgContent = $msgContent;
    }

    public function getMsgContent() {
        return $this->msgContent;
    }

    public function setAnswer($answer) {
        $this->answer = $answer;
    }

    public function getAnswer() {
        return $this->answer;
    }

    public function setFormType($formType) {
        $this->formType = $formType;
    }

    public function getFormType() {
        return $this->formType;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function getStatus() {
        return $this->status;
    }

    public function contactUsForm() {
        $this->setUserID($_SESSION['userID']);
        $this->setName($_SESSION['name']);
        $this->setMsgContent($_SESSION['comments']);
        $this->setStatus($_SESSION['status']);

//        $this->setEmail($_SESSION['email']);
//        $this->setAnswer($_SESSION['answer']);
//        $this->setFormType($_SESSION['formType']);


        unset($_SESSION['name']);
        unset($_SESSION['comments']);
        unset($_SESSION['status']);

//        unset($_SESSION['email']);
//        unset($_SESSION['answer']);
//        unset($_SESSION['formType']);
        //CHECK FOR THE NEXT MESSAGEID 
        $sql = "SELECT messageID from messages ORDER BY messageID DESC LIMIT 1";
        $qRes = sqlsrv_query($this->conn, $sql);


        if ($qRes->num_rows > 0) {
            while ($row = sqlsrv_fetch_array($qRes)) {
                $msgID = $row['messageID'] + 1;
            }
        }
        else {
            $msgID = 1;
        }

        if ($msgID != NULL || $msgID != '') {
            $senderID = $this->getUserID();
            $senderName = $this->getName();
            $msgContent = $this->getMsgContent();
            $msgStatus = $this->getStatus();
            $threadID = $msgID . $senderID;

            $SqlSID = $this->conn->real_escape_string($senderID);
            //$SqlSName = $this->conn->real_escape_string($senderName);
            $SqlMsgContent = $this->conn->real_escape_string($msgContent);
            $SqlThreadID = $this->conn->real_escape_string($threadID);


            $sql2 = "INSERT INTO messages (senderID, senderName ,msgContent, threadID, msgStatus) VALUES('$SqlSID', '$senderName', '$SqlMsgContent', '$SqlThreadID', '$msgStatus')";


            $qRes2 = @sqlsrv_query($this->conn, $sql2);
            if ($qRes2 === FALSE) {
                echo "<script>alert('There has been an error sending message. Please try again." . "Error message" . $this->conn->connect_errno . ": " . $this->conn->connect_error . "');</script>";

                $this->failedRedirect();
            }
            else {
                $this->checkThreadID();
//                $this->successRedirect();
            }
        }
    }

//        $sql = "SELECT * FROM forms";
//
//
//        $result = @sqlsrv_query($this->conn, $sql);
//        if ($result !== FALSE) {
//            while (($row = sqlsrv_fetch_array($result)) != FALSE) {
//                if (
//                        $this->getName() != $row['name'] ||
//                        $this->getEmail() != $row['Email'] ||
//                        $this->getMsgContent() != $row['msgContent'] ||
//                        $this->getAnswer() != $row['answer'] ||
//                        $this->getStatus() != $row['status'] ||
//                        $this->getFormType() != $row['formType']
//                ) {
//                    continue;
//                }
//                else {
//                    $this->successRedirect();
//                }
//            }
//            
//            $sql1 = "INSERT INTO forms" .
//                    " (userID, msgContent, answer, status, formType) " .
//                    " VALUES " .
//                    " ('" . $this->getUserID() . "', '" . $this->getMsgContent() . "', '" . $this->getAnswer() . "', '" . $this->getStatus() . "', '" . $this->getFormType() . "')";
//
//            $result1 = sqlsrv_query($this->conn, $sql1);
//
//            if ($result1 === FALSE)
//                
//                echo "Unable to execute the query"
//                . $this->conn->connect_errno
//                . $this->conn->connect_error
//                . $sql1 ;
//
//
//
//            else {
//
//                $this->successRedirect();
//            }
//        }
//        else {
//            echo "Unable to execute the query."
//            . $this->conn->connect_errno
//            . $this->conn->connect_error;
//        }


    function checkThreadID() {
        //CHECK IF THREAD ID AND MESSAGE ID MATCHES 
        $sql3 = "SELECT * FROM messages ORDER BY messageID DESC LIMIT 1 ";
        $qRes3 = @sqlsrv_query($this->conn, $sql3);


        while ($row3 = sqlsrv_fetch_array($qRes3)) {
            $cMessageID = $row3['messageID'];
            $cUserID = $row3['senderID'];
            $cThreadID = $row3['messageID'] . $row3['senderID'];

            if ($row3['threadID'] != $cThreadID) {
                $sql4 = "UPDATE messages SET threadID = '$cThreadID' WHERE messageID = '" . $row3['messageID'] . "'";
                $qRes4 = @sqlsrv_query($this->conn, $sql4);

                if ($qRes4 === FALSE) {
                    $this->failedRedirect();
                }
                else {
                    $this->successRedirect();
                }
            }
            else{
                $this->successRedirect();
            }
        }
        //$this->successRedirect();
    }

    function successRedirect() {
        ob_start();
        header("refresh:0;url=index.php");
        echo "<script>alert('Your enquiry has been sent.')</script>";
        ob_end_flush();
        die();
    }

    function failedRedirect() {
        ob_start();
        header("refresh:0;url=contactUsUI.php");
        echo "<script>alert('Enquiry Fail')</script>";
        ob_end_flush();
        die();
    }

    function __wakeup() {
        include("SIMOpenHouseDb.php");
        $this->conn = $conn;
    }

    function __destruct() {
        if (!$this->conn->connect_error)
            @$this->conn->close();
    }

}
?>

<html>
    <body>

        <?php
        $contactUsF = new ContactUs();

        $contactUsF->contactUsForm();
        ?>
    </body>
</html>