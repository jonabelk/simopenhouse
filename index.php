<!DOCTYPE html>

<?php
    session_start();
    ob_start();
    include("navbarUI.php");
?>


<html>
    <head>
        <title>SIM Open House 2021</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        
        <link rel="stylesheet" href="css/main.css">
    </head>
    <body>
    <!-- BEGIN CAROUSEL -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <img src="http://1.bp.blogspot.com/-pQ2NIuI1KFM/UELhVAOnZSI/AAAAAAAAALQ/MEMAfk7t6yU/s1600/SIM+Open+Hse+Sep+2012.jpg" alt="Image">
            <div class="carousel-caption">
              <h3><!-- enter text later--></h3>
              <p><!-- enter text later--></p>
            </div>      
          </div>

          <div class="item">
            <img src="https://www.simge.edu.sg/wp-content/uploads/2019/11/11-2818-BMR-JBO-JanFeb20-Web-Mega-Event-Banner1440x600_jx_v03.jpg" alt="Image">
            <div class="carousel-caption">
              <h3><!-- enter text later--></h3>
              <p><!-- enter text later--></p>
            </div>      
          </div>
      
        
        <div class="item">
            <img src="images/image3.jpg" alt="Image">
            <div class="carousel-caption">
              <h3><!-- enter text later--></h3>
              <p><!-- enter text later--></p>
            </div>      
          </div>
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
    </div>        
    <!-- END CAROUSEL -->    
    <br/>    
    
    <!-- BEGIN DISPLAYING BOTTOM DESC -->
    <div class="container marketing">
        <!-- Three columns of text below the carousel -->
        <div class="row">
          <div class="col-lg-4">
            <img class="img-responsive center-block" src="images/studentlife.png" alt="Generic placeholder image" width="350">
            <p>Life is so much more than just books, exams and qualifications. This is especially true if you are a student at SIM Global Education. To us, your education encompasses a broad spectrum of knowledge, skills and activities; both inside and outside of the typical classroom. Read on and learn more about the many facets of student life in SIM GE.</p>
            <p><a class="btn btn-secondary" href="studentLifeUI.php" role="button"style="margin-left:-10px;">View details &raquo;</a></p>
          </div><!-- /.col-lg-4 -->
          <div class="col-lg-4">
            <img class="img-responsive center-block"src="images/ourprogrammes.png" alt="Generic placeholder image" width="350">
            
            <p>SIM offers a wide and diverse choice of full-time programmes in Singapore that range from diploma to undergraduate and postgraduate degree programmes.These programmes provide you with a seamless education pathway to fulfilling your aspirations.</p>
            <p><a class="btn btn-secondary" href="viewAllProgUI.php" role="button"style="margin-left:-10px;">View details &raquo;</a></p>
          </div><!-- /.col-lg-4 -->
          <div class="col-lg-4">
            <img class="img-responsive center-block"src="images/upcomingevents.png" alt="Generic placeholder image" width="350">
            <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
            <p><a class="btn btn-secondary" href="viewAllEventsUI.php" role="button" style="margin-left:-10px;">View details &raquo;</a></p>
          </div><!-- /.col-lg-4 -->
        </div><!-- /.row -->


        <!-- START THE FEATURETTES -->

        <hr class="featurette-divider">

        <div class="row featurette">
          <div class="col-md-7">
            <h2 class="featurette-heading">About Us</h2>
            <p class="lead" style="font-size: 15px;">SIM Global Education (SIM GE) is a leading private education institution in Singapore. We offer more than 80 academic programmes ranging from our own diploma and graduate diploma courses to Bachelor’s and Master’s programmes with top ranking and reputable universities from Australia, Europe, the United Kingdom and the United States. Our current enrolment stands at about 16,000 full and part-time students and adult learners. About 25% of our full-time students are international students hailing from over 40 countries.

    SIM GE’s holistic learning approach and culturally diverse learning environment aim to equip students with an all-rounded global education. Besides building competencies and skills needed to thrive in the real world, SIM GE also hones the soft skills of students through a wide range of student life, career development and networking activities.</p>
          </div>
          <div class="col-md-5">
            <img class="img-responsive featurette-image img-fluid mx-auto" src="https://www.sim.edu.sg/Discover-SIM/PublishingImages/Clementi_HQ.jpg" alt="Image" width="500px">
          </div>
        </div>

        <hr class="featurette-divider">

        <div class="row featurette">
          <div class="col-md-7 order-md-2">
            <h2 class="featurette-heading">Our University Partners</h2>
            <p class="lead" style="font-size: 15px;">SIM GE offers over 80 academic programmes ranging from diploma to bachelor and postgraduate degrees,
from some of the finest universities across the globe.</p>
          </div>
          <div class="col-md-5 order-md-1">
            <img class="img-responsive featurette-image img-fluid mx-auto " src="https://www.simge.edu.sg/wp-content/uploads/2021/01/UP-Logo-Set.png" alt="Image" width="500px">
          </div>
        </div>

        <hr class="featurette-divider">

        <div class="row featurette">
          <div class="col-md-7">
            <h2 class="featurette-heading">Hear From Our Students!</h2>
            <p class="lead" style="font-size: 15px;">"The courage to steer away from a familiar path to nurture an underlying passion, or pursue an interest discovered midway is laudable. What better way to appreciate this than hearing the real stories of those who have self-discovered, recalibrated and made informed shifts towards fulfilling careers of their yearning? Hear first-hand from former engineering students Marcus Chee and Jonah Foong, who graduated with First Class Honours, from the University of London’s BSc (Hons) in International Relations (BIR) programme at SIM."<i><strong> -- Marcus Chee/Jonah Foong</strong></i></p>
          </div>
          <div class="col-md-5">
            <img class="img-responsive featurette-image img-fluid mx-auto" src="https://www.simge.edu.sg/wp-content/uploads/2021/03/Jonah-Marcus-Thumbnail-1.png" alt="Image" width="300px">
          </div>
        </div>

        <hr class="featurette-divider">

        <!-- /END THE FEATURETTES -->

      </div><!-- /.container -->
   

    
    <!-- END OF DISPLAYING BOTTOM DESC -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../../assets/js/vendor/popper.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    
    </body>
    <footer class="container-fluid text-center">
        <?php include("footerUI.php"); ?>
    </footer>
    
    
</html>